#Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.7.1]
### Added
- job cleaning up unused tokens
### Changed
- small cosmetic changes + bugfixes

## [0.7.0]
### Added
- timesheet resource
- token resource
- auth framework rework

## [0.6.0]
### Added
- task resource
- assignee and manager resource

## [0.5.0]
### Added
- project resource
### Performance
- bumped fragaria-webapp to AOT compiled version
### Security
- added possibility and sample config for http2

## [0.4.1]
### Changed
- gzip resources by default

## [0.4.0] - PHASE 2
### Changed
- new text based migration engine
### Removed
- Hello World resource removed
### Security
- User hash leaking for logged user fixed

## [0.3.0]
### Added
- added more parameters to user: nick, enabled, full name
- patch user interface added
### Changed
- default configuration improvements/cleanup
### Removed
- removed permissions resource in admin panel

## [0.2.0]
### Added
- added account resource, user can change password

## [0.1.0]
### Added
- initial work
