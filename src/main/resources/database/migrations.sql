#1
CREATE CLASS DatabaseVersion extends V
CREATE PROPERTY DatabaseVersion.version SHORT ( MANDATORY TRUE, NOTNULL, MIN 0)

#2
CREATE CLASS User extends V
CREATE PROPERTY User.login STRING (MANDATORY TRUE, NOTNULL)
CREATE PROPERTY User.perm EMBEDDEDSET STRING (MANDATORY TRUE, NOTNULL)
CREATE INDEX User.login UNIQUE_HASH_INDEX
CREATE INDEX User_Login_Perm on User(login, perm) UNIQUE

#3
CREATE CLASS Token extends V
CREATE PROPERTY Token.UserAgent STRING (MANDATORY TRUE, NOTNULL, MAX 150)
CREATE PROPERTY Token.TokenHash STRING (MANDATORY TRUE, NOTNULL, MAX 150)
CREATE PROPERTY Token.valid BOOLEAN (MANDATORY TRUE, NOTNULL, DEFAULT TRUE)
CREATE PROPERTY Token.LastAccess LONG (MANDATORY TRUE, NOTNULL, MIN 0)
CREATE PROPERTY Token.ExpTime LONG (MANDATORY TRUE, NOTNULL, MIN 0)
CREATE PROPERTY Token.Source STRING (MANDATORY TRUE, NOTNULL, MAX 150)

CREATE INDEX Token.TokenHash UNIQUE_HASH_INDEX

CREATE CLASS hasSession extends E
CREATE PROPERTY hasSession.out LINK Token
CREATE PROPERTY hasSession.in LINK User

#4
CREATE VERTEX User set login = 'admin', email='admin@gmail.com', fullName = 'Admin - konto funkcyjne', password = '<password:admin>', perm = ['A', 'U']

#5:DEMO
CREATE VERTEX User set login = 'john', email = 'john@gmail.com', fullName = 'John Shephard', password = '<password:password>', perm = ['U']
CREATE VERTEX User set login = 'mary', email = 'mary@gmail.com', fullName = 'Mary Poppins', password = '<password:password>', perm = ['U']
CREATE VERTEX User set login = 'duke', email = 'duke@gmail.com', fullName = 'Duke Hufflepuff', password = '<password:password>', perm = ['U']
CREATE VERTEX User set login = 'lars', email = 'lars@gmail.com', password = '<password:password>', perm = ['U']

#6
CREATE CLASS Assignable extends V ABSTRACT

CREATE CLASS Project extends Assignable
CREATE PROPERTY Project.id STRING (MANDATORY TRUE, NOTNULL, MAX 10)
CREATE PROPERTY Project.title STRING (MANDATORY TRUE, NOTNULL, MAX 215)

CREATE PROPERTY Project.createTaskStrategy STRING (MANDATORY TRUE, NOTNULL, MAX 20, DEFAULT "ASSIGNED")
CREATE PROPERTY Project.visibilityStrategy STRING (MANDATORY TRUE, NOTNULL, MAX 20, DEFAULT "ASSIGNED")
CREATE PROPERTY Project.loggingWhoStrategy STRING (MANDATORY TRUE, NOTNULL, MAX 20, DEFAULT "ASSIGNED")
CREATE PROPERTY Project.loggingWhenStrategy STRING (MANDATORY TRUE, NOTNULL, MAX 20, DEFAULT "WEEKLY")

CREATE PROPERTY Project.status STRING (MANDATORY TRUE, NOTNULL, MAX 10, DEFAULT "OPEN")

CREATE PROPERTY Project.totalLogged INTEGER (MANDATORY TRUE, NOTNULL, MIN 0, DEFAULT 0)
CREATE PROPERTY Project.estimatedTime INTEGER (MANDATORY TRUE, NOTNULL, MIN 0, DEFAULT 0)
CREATE PROPERTY Project.startDate DATE
CREATE PROPERTY Project.finishDate DATE

ALTER PROPERTY Project.status REGEXP "OPEN|CLOSED|PENDING"
ALTER PROPERTY Project.createTaskStrategy REGEXP "ASSIGNED|MANAGER|ALL"
ALTER PROPERTY Project.visibilityStrategy REGEXP "ASSIGNED|ALL"
ALTER PROPERTY Project.loggingWhoStrategy REGEXP "ASSIGNED|ALL"
ALTER PROPERTY Project.loggingWhenStrategy REGEXP "WEEKLY|MONTHLY|WEEKLY_MONTHLY"

CREATE PROPERTY Project.description STRING (MAX 2047)
CREATE PROPERTY Project.seq SHORT ( MANDATORY TRUE, NOTNULL, MIN 0, DEFAULT 0)

CREATE INDEX Project.id UNIQUE
CREATE INDEX ProjectIndex ON Project (id, status) UNIQUE
CREATE INDEX UniqueProjectTitle on Project(title) UNIQUE
CREATE INDEX Project.title FULLTEXT

CREATE CLASS Task extends Assignable
CREATE PROPERTY Task.projectId STRING (MANDATORY TRUE, NOTNULL, MAX 20)
CREATE PROPERTY Task.title STRING (MANDATORY TRUE, NOTNULL, MAX 215)
CREATE PROPERTY Task.description STRING (MAX 2047)
CREATE PROPERTY Task.seq SHORT ( MANDATORY TRUE, NOTNULL, MIN 0)
CREATE PROPERTY Task.status STRING (MANDATORY TRUE, NOTNULL, MAX 10, DEFAULT "OPEN")
ALTER PROPERTY Task.status REGEXP "OPEN|CLOSED|PENDING"

CREATE PROPERTY Task.totalLogged INTEGER (MANDATORY TRUE, NOTNULL, MIN 0, DEFAULT 0)
CREATE PROPERTY Task.estimatedTime INTEGER (MANDATORY TRUE, NOTNULL, MIN 0, DEFAULT 0)
CREATE PROPERTY Task.startDate DATE
CREATE PROPERTY Task.finishDate DATE

CREATE PROPERTY Task.created DATE
CREATE PROPERTY Task.updated DATE

CREATE INDEX TaskPK ON Task (projectId, seq) UNIQUE
CREATE INDEX Task.title FULLTEXT
CREATE INDEX UniqueTaskTitle ON Task(projectId, title) UNIQUE

CREATE CLASS hasTask extends E
CREATE PROPERTY hasTask.out LINK Project
CREATE PROPERTY hasTask.in LINK Task
CREATE INDEX UniqueHasTask ON hasTask(out,in) UNIQUE

CREATE CLASS assignEdge extends E ABSTRACT
CREATE PROPERTY assignEdge.out LINK Assignable
CREATE PROPERTY assignEdge.in LINK User

CREATE CLASS hasAssignee extends assignEdge
CREATE INDEX UniqueHasAssignee ON hasAssignee(out,in) UNIQUE

CREATE CLASS hasManager extends assignEdge
CREATE INDEX UniqueHasManager ON hasManager(out,in) UNIQUE

#7:DEMO
CREATE VERTEX Project set id = 'KSG2016', seq=5, title='Księgowość 2016', status = 'CLOSED'
CREATE VERTEX Project set id = 'KSG2017', seq=5, title='Księgowość 2017', status = 'PENDING'
CREATE VERTEX Project set id = 'BULB', seq=3, title='Bulbulator', status = 'OPEN'
CREATE VERTEX Project set id = 'FRG', seq=4, title='Fragaria', description = 'Lorem ipsum', status = 'OPEN'
CREATE VERTEX Project set id = 'KANSS', seq=0, title='Kanalizacja - obiekt SS', status = 'PENDING'

CREATE EDGE hasManager FROM ( SELECT FROM PROJECT ) TO ( SELECT FROM User WHERE login = "john")
CREATE EDGE hasAssignee FROM ( SELECT FROM PROJECT ) TO ( SELECT FROM User WHERE login = "mary")
CREATE EDGE hasAssignee FROM ( SELECT FROM PROJECT WHERE id = 'BULB' ) TO ( SELECT FROM User WHERE login = "lars")

CREATE VERTEX Task set projectId = 'KSG2016', seq=1, title='Bilans Księgowy', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2016', seq=2, title='Rozrachunki', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2016', seq=3,title='Zarządzanie Budynkiem', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2016', seq=4, title='Rozliczanie czasu pracy', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2016', seq=5, title='Rozliczanie kasy', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()

CREATE EDGE hasTask FROM (SELECT FROM Project WHERE id = 'KSG2016') TO (SELECT FROM Task WHERE projectId = "KSG2016")

CREATE VERTEX Task set projectId = 'KSG2017', seq=1, title='Bilans Księgowy', status = 'PENDING', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2017', seq=2, title='Rozrachunki', status = 'PENDING', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2017', seq=3, title='Zarządzanie Budynkiem', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2017', seq=4, title='Rozliczanie czasu pracy', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'KSG2017', seq=5, title='Rozliczanie kasy', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()

CREATE EDGE hasTask FROM (SELECT FROM Project WHERE id = 'KSG2017') TO (select * from Task where projectId = "KSG2017" )

CREATE VERTEX Task set projectId = 'BULB', seq=1, title='Projekt Automatyki', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'BULB', seq=2, title='Delegacje', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'BULB', seq=3, title='Projekt techniczny', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()

CREATE EDGE hasTask FROM (SELECT FROM Project WHERE id = 'BULB') TO (select * from Task where projectId = "BULB")

CREATE VERTEX Task set projectId = 'FRG', seq=1, title='Setup systemu do budowy aplikacji', status = 'CLOSED', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'FRG', seq=2, title='Budowa części serwerowej', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'FRG', seq=3, title='Budowa aplikacji sieciowej', status = 'OPEN', created= SYSDATE (), updated = SYSDATE()
CREATE VERTEX Task set projectId = 'FRG', seq=4, title='Poprawa UX', status = 'PENDING', created= SYSDATE (), updated = SYSDATE()

CREATE EDGE hasTask FROM (SELECT FROM Project WHERE id = 'FRG') TO (select * from Task where projectId = "FRG")

#8
CREATE CLASS workedOn extends E

CREATE PROPERTY workedOn.dt DATE
CREATE PROPERTY workedOn.time SHORT
CREATE PROPERTY workedOn.in LINK User
CREATE PROPERTY workedOn.out LINK Task

CREATE INDEX UniqueTimeEntry ON workedOn(dt, in, out) UNIQUE
