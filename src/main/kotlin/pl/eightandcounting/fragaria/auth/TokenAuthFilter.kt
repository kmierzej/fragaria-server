package pl.eightandcounting.fragaria.auth

import io.dropwizard.auth.AuthFilter
import io.dropwizard.auth.AuthenticationException
import java.security.Principal
import javax.annotation.Priority
import javax.ws.rs.Priorities
import javax.ws.rs.WebApplicationException
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.core.SecurityContext

@Priority(Priorities.AUTHENTICATION)
class TokenAuthFilter <P:Principal> : AuthFilter<String, P>(){
    override fun filter(requestContext: ContainerRequestContext) {
        logger.debug("Token Auth Filter In Progress!")

        val authToken = requestContext.headers.getFirst("authToken")

        try {
            if (!authToken.isNullOrBlank()) {
                try {
                    val principal = authenticator.authenticate(authToken).orElse(null)
                    if (principal != null) {
                        requestContext.securityContext = object : SecurityContext {
                            override fun getAuthenticationScheme(): String = "TOKEN"
                            override fun getUserPrincipal(): Principal = principal
                            override fun isSecure(): Boolean = requestContext.securityContext.isSecure
                            override fun isUserInRole(role: String): Boolean = authorizer.authorize(principal, role)
                        }
                        return
                    }
                } catch (e: AuthenticationException) {
                    logger.warn("Error authenticating credentials", e)
                }
            }
        } catch (e : IllegalArgumentException) {
            logger.warn("Error decoding credentials", e)
        }

        throw WebApplicationException(unauthorizedHandler.buildResponse(prefix, realm))
    }

    class Builder<P : Principal> : AuthFilterBuilder<String, P, TokenAuthFilter<P>>() {
        override fun newInstance(): TokenAuthFilter<P> = TokenAuthFilter()
    }
}
