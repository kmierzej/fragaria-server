package pl.eightandcounting.fragaria.auth

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import io.dropwizard.auth.Authenticator
import io.dropwizard.auth.Authorizer

val authModule = Kodein.Module {
    bind<Authorizer<TokenPrincipal>>() with singleton { UserRoleAuthorizer() }
    bind<Authenticator<String, TokenPrincipal>>() with singleton { AuthTokenAuthenticator(instance()) }
}
