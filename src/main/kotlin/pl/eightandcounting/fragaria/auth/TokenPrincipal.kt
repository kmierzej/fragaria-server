package pl.eightandcounting.fragaria.auth

import pl.eightandcounting.fragaria.api.Permissions
import java.security.Principal

data class TokenPrincipal(
    val login: String,
    val permissions: Collection<Permissions>
) : Principal {
    override fun getName(): String = login

    fun hasActiveAdminPermission(): Boolean {
        return permissions.contains(Permissions.ADMIN)
    }
}
