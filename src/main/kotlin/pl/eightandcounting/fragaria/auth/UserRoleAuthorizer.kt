package pl.eightandcounting.fragaria.auth

import io.dropwizard.auth.Authorizer
import org.slf4j.LoggerFactory
import pl.eightandcounting.fragaria.api.Permissions

class UserRoleAuthorizer : Authorizer<TokenPrincipal>{
    private val logger = LoggerFactory.getLogger(this::class.java)
    override fun authorize(user: TokenPrincipal?, role: String?): Boolean {
        if(user == null) {
            return false
        } else if( user.permissions.isEmpty()) {
            return false
        } else if(role == null || role.isEmpty() ) {
           return false
        }

        // logger.info("Attempted to authorise access as $role")
        val privilege = extractPrivilege(role) ?: return false
        return user.permissions.contains(privilege)
    }

    private fun extractPrivilege(role: String): Permissions? {
        try {
            return Permissions.valueOf(role)
        } catch (e : IllegalArgumentException) {
            logger.error("Unable to authorize sice role $role is not a valid role" )
            return null
        }
    }
}
