package pl.eightandcounting.fragaria.auth

import io.dropwizard.auth.UnauthorizedHandler
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import javax.ws.rs.core.Response

class JsonUnauthorizedHandler: UnauthorizedHandler {
    override fun buildResponse(prefix: String, realm: String): Response {
        return ErrorMessageBasic.AUTH_ERROR.toResponse()
    }
}
