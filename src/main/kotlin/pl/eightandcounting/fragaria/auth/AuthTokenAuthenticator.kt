package pl.eightandcounting.fragaria.auth

import io.dropwizard.auth.Authenticator
import pl.eightandcounting.fragaria.services.token.TokenReaderService
import java.util.*

class AuthTokenAuthenticator (private val tokenReaderService: TokenReaderService): Authenticator<String, TokenPrincipal> {
    override fun authenticate(authToken: String): Optional<TokenPrincipal> {
        val data = tokenReaderService.validateAccessToken(authToken) ?: return Optional.empty()
        return Optional.of(TokenPrincipal(data.login, data.permissions))
    }
}
