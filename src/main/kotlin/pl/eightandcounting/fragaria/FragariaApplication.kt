package pl.eightandcounting.fragaria

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.salomonbrys.kodein.*
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import io.dropwizard.Application
import io.dropwizard.assets.AssetsBundle
import io.dropwizard.auth.AuthDynamicFeature
import io.dropwizard.auth.AuthValueFactoryProvider
import io.dropwizard.jersey.setup.JerseyEnvironment
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.process.internal.RequestScoped
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature
import org.knowm.dropwizard.sundial.SundialBundle
import org.knowm.dropwizard.sundial.SundialConfiguration
import pl.eightandcounting.fragaria.auth.JsonUnauthorizedHandler
import pl.eightandcounting.fragaria.auth.TokenAuthFilter
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.filters.CacheControlFilter
import pl.eightandcounting.fragaria.migrations.DatabaseMigrator
import pl.eightandcounting.fragaria.resources.*
import ru.vyarus.dropwizard.orient.OrientServerBundle
import java.util.*
import javax.servlet.DispatcherType

class FragariaApplication : Application<FragariaConfiguration>() {

    override fun getName(): String {
        return "Fragaria"
    }

    override fun initialize(bootstrap: Bootstrap<FragariaConfiguration>) {
        bootstrap.addBundle(OrientServerBundle(configurationClass))
        bootstrap.addBundle(AssetsBundle("/META-INF/resources/fragaria/webapp", "/", "index.html"))
        bootstrap.addBundle(getSundialBundle())
    }

    private fun getSundialBundle(): SundialBundle<FragariaConfiguration> {
        return object: SundialBundle<FragariaConfiguration>(){
            override fun getSundialConfiguration(config: FragariaConfiguration): SundialConfiguration {
                return config.sundialConfiguration
            }
        }
    }

    override fun run(config: FragariaConfiguration, env: Environment) {
        if(config.enableCors) {
            configureCors(env)
        }

        val database = setupDatabase(config)
        val injector = setupInjector(database, config)

        env.applicationContext.setAttribute("INJECTOR", injector)

        registerResources(env.jersey(), injector)
        configureObjectMapper(env.objectMapper)
        registerAuthentication(env, injector)
    }

    private fun setupInjector(db: OrientGraphFactory, config: FragariaConfiguration): Kodein {
        return Kodein {
            bind<OrientGraphFactory>() with instance(db)
            constant("secret") with config.secret
            constant("tokenStrategy") with config.tokenStrategy

            import(appModule)
        }
    }

    private fun registerResources(jersey: JerseyEnvironment, injector: Kodein) {
        jersey.register(injector.provider<LoginResource>().invoke())
        jersey.register(injector.provider<ManageUsersResource>().invoke())
        jersey.register(injector.provider<ManageProjectResource>().invoke())
        jersey.register(injector.provider<ProjectAssigneeResource>().invoke())
        jersey.register(injector.provider<ManageTaskResource>().invoke())
        jersey.register(injector.provider<ManageAccountResource>().invoke())
        jersey.register(injector.provider<TimesheetResource>().invoke())

        jersey.register(object: AbstractBinder(){
            override fun configure() {
                bindFactory(RequestAddressFactory::class.java)
                    .to(RequestAddress::class.java)
                    .proxy(false)
                    .proxyForSameScope(false)
                    .`in`(RequestScoped::class.java)
            }
        })
    }

    private fun configureObjectMapper(objectMapper: ObjectMapper) {
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        objectMapper.registerKotlinModule()
    }

    private fun registerAuthentication(env: Environment, kodein: Kodein) {
        val tokenAuthFilter = buildAuthFilter(kodein)

        val jersey = env.jersey()
        jersey.register(RolesAllowedDynamicFeature::class.java)
        jersey.register(AuthDynamicFeature(tokenAuthFilter))
        jersey.register(AuthValueFactoryProvider.Binder<TokenPrincipal>(TokenPrincipal::class.java))

        val cacheFilter = env.servlets().addFilter("CacheControlFilter", CacheControlFilter())
        cacheFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*")
    }

    private fun buildAuthFilter(kodein: Kodein): TokenAuthFilter<TokenPrincipal> {
        return TokenAuthFilter.Builder<TokenPrincipal>()
            .setAuthorizer(kodein.instance())
            .setAuthenticator(kodein.instance())
            .setUnauthorizedHandler(JsonUnauthorizedHandler())
            .buildAuthFilter()
    }

    private fun setupDatabase(config: FragariaConfiguration): OrientGraphFactory {
        val dbPath = config.orientServerConfiguration.filesPath
        val dbURL = "plocal:$dbPath/databases/fragaria"
        val database = OrientGraphFactory(dbURL, true)

        val migrator = DatabaseMigrator(config.dataset)
        return migrator.migrateDatabase(database)
    }

    private fun configureCors(environment: Environment) {
        val urlPattern = environment.applicationContext.contextPath + "*"
        val dispatcherTypes = EnumSet.of(DispatcherType.REQUEST)

        val cors = environment.servlets().addFilter("CORS", CrossOriginFilter::class.java)
        cors.addMappingForUrlPatterns(dispatcherTypes, false, urlPattern)
        cors.setInitParameters(getInitCorsParameters())
    }

    private fun getInitCorsParameters() : Map<String, String> = mapOf(
        CrossOriginFilter.ALLOWED_METHODS_PARAM to "GET,PUT,POST,PATCH,DELETE,OPTIONS",
        CrossOriginFilter.ALLOWED_ORIGINS_PARAM to "*",
        CrossOriginFilter.ALLOWED_HEADERS_PARAM to "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,authToken",
        CrossOriginFilter.ALLOW_CREDENTIALS_PARAM to "true"
    )
}
