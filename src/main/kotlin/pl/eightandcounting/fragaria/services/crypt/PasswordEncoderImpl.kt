package pl.eightandcounting.fragaria.services.crypt

import org.mindrot.jbcrypt.BCrypt


class PasswordEncoderImpl: PasswordEncoder {
    override fun encode(password: String): String {
        val trimmedPass = password.trim()
        //if(password.length < 6) throw ErrorMessageWebException(ErrorMessage.PASSWORD_IS_TOO_SHORT)
        return BCrypt.hashpw(trimmedPass, BCrypt.gensalt())
    }
}
