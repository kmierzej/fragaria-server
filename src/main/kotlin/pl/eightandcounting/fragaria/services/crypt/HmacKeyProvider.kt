package pl.eightandcounting.fragaria.services.crypt

import org.jose4j.keys.HmacKey
import org.slf4j.LoggerFactory
import java.security.SecureRandom

class HmacKeyProvider(secret: String) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    data class HmacPair(val refreshToken: HmacKey, val accessToken: HmacKey)
    private val hmacPair = HmacPair(
        refreshToken = computeHmac(secret),
        accessToken = computeRandomHmac()
    )

    fun provide(): HmacPair = hmacPair

    private fun computeHmac(secret: String): HmacKey {
        if(secret.isBlank()) {
            logger.info("Computing Random Hmac - secret not provided")
            return computeRandomHmac()
        } else {
            return HmacKey(secret.take(64).padEnd(64, '0').toByteArray())
        }
    }

    private fun computeRandomHmac(): HmacKey {
        val array = ByteArray(64)
        SecureRandom().nextBytes(array)
        return HmacKey(array)
    }
}
