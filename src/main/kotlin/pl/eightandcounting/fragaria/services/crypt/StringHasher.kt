package pl.eightandcounting.fragaria.services.crypt

interface StringHasher {
    fun hashString(string: String): String
}

