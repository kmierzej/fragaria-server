package pl.eightandcounting.fragaria.services.crypt

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

class StringHasherImpl: StringHasher {
    private val sha = MessageDigest.getInstance("SHA")

    override fun hashString(string: String): String {
        return DatatypeConverter.printHexBinary(sha.digest(string.toByteArray()))
    }
}
