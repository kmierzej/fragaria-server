package pl.eightandcounting.fragaria.services.crypt

interface PasswordEncoder {
    fun encode(password: String): String
}
