package pl.eightandcounting.fragaria.services.token

import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jws.AlgorithmIdentifiers
import pl.eightandcounting.fragaria.services.crypt.HmacKeyProvider.HmacPair
import pl.eightandcounting.fragaria.services.token.model.TokenClaims

class TokenReaderServiceImpl (
    private val key: HmacPair,
    private val dateProvider: () -> org.jose4j.jwt.NumericDate
): TokenReaderService {
    private val algorithmConstraints = org.jose4j.jwa.AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, AlgorithmIdentifiers.HMAC_SHA512)

    override fun validateAccessToken(token: String): pl.eightandcounting.fragaria.model.auth.TokenCredentials? {
        try {
            val claims = extractClaimsFromToken(token, key.accessToken)
            return extractCredentialsFromClaim(claims)
        } catch (e: org.jose4j.jwt.consumer.InvalidJwtException) {
            return null
        }
    }

    override fun validateRefreshToken(token: String): pl.eightandcounting.fragaria.model.auth.TokenCredentials? {
        try {
            val claims = extractClaimsFromToken(token, key.refreshToken)
            return extractCredentialsFromClaim(claims)
        } catch (e: org.jose4j.jwt.consumer.InvalidJwtException) {
            return null
        }
    }

    private fun extractCredentialsFromClaim(claims: TokenClaims): pl.eightandcounting.fragaria.model.auth.TokenCredentials {
        val permissions = claims.getPrivileges().map { pl.eightandcounting.fragaria.api.Permissions.Companion.getPermission(it) }.filterNotNull().toSet()
        val login = claims.getLogin()
        val expirationTime = claims.getExpirationTime().valueInMillis
        return pl.eightandcounting.fragaria.model.auth.TokenCredentials(permissions = permissions, login = login, expirationTime = expirationTime)
    }

    private fun extractClaimsFromToken(token: String, key: org.jose4j.keys.HmacKey): TokenClaims {
        val jwtConsumer = getConsumer(key)
        val claims = jwtConsumer.processToClaims(token)
        return TokenClaims(claims)
    }

    private fun getConsumer(key: org.jose4j.keys.HmacKey): org.jose4j.jwt.consumer.JwtConsumer {
        val now = dateProvider.invoke()
        return org.jose4j.jwt.consumer.JwtConsumerBuilder()
            .setEvaluationTime(now)
            .setVerificationKey(key)
            .setJwsAlgorithmConstraints(algorithmConstraints)
            .build()
    }
}
