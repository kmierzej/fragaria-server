package pl.eightandcounting.fragaria.services.token

import org.jose4j.jwt.NumericDate
import org.jose4j.keys.HmacKey
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.model.auth.Token
import pl.eightandcounting.fragaria.model.auth.TokenPair
import pl.eightandcounting.fragaria.services.crypt.HmacKeyProvider.HmacPair
import pl.eightandcounting.fragaria.services.token.model.TokenClaims
import utils.addHours
import utils.addMinutes

class TokenBuilderServiceImpl(
    private val key: HmacPair,
    private val dateProvider: () -> NumericDate,
    private val DEBUG_MODE: Boolean = false
) : TokenBuilderService {
    private val REFRESH_TOKEN_DURATION_HOURS: Long = 24
    private val ACCESS_TOKEN_DURATION_MINUTES: Long = 5

    override fun refreshRefreshToken(login: String, permissions: Set<Permissions>, immediatePermissions: Set<Permissions>): TokenPair {
        val now = dateProvider.invoke()

        val refreshToken = createRefreshToken(login, now, permissions)
        val accessTokens = immediatePermissions.map { createAccessToken(login, now, it) }
        return TokenPair(accessTokens = accessTokens, refreshToken = refreshToken)
    }

    override fun createAccessTokens(login: String, permissions: Set<Permissions>): List<Token> {
        return permissions.map { createAccessToken(login, dateProvider.invoke(), it) }
    }

    private fun createRefreshToken(login: String, from: NumericDate, permissions: Set<Permissions>): Token {
        val expirationTime = from.addHours(REFRESH_TOKEN_DURATION_HOURS)
        val claims = getClaims(login, from, expirationTime, permissions)
        val token = claims.signClaims(key.refreshToken)
        return Token(token, expirationTime.valueInMillis, permissions)
    }

    private fun createAccessToken(login: String, from: NumericDate, permission: Permissions): Token {
        val expirationTime = from.addMinutes(ACCESS_TOKEN_DURATION_MINUTES)
        val claims = getClaims(login, from, expirationTime, setOf(permission))
        val token = claims.signClaims(key.accessToken)
        return Token(token, expirationTime.valueInMillis, setOf(permission))
    }

    private fun getClaims(login: String, from: NumericDate, expirationTime: NumericDate, permissions: Collection<Permissions>): TokenClaims {
        val claims = TokenClaims()
        claims.setLogin(login)
        claims.setIssuedAt(from)
        claims.setNotBefore(from.addMinutes(-2))
        claims.setExpirationTime(expirationTime)
        claims.setPrivileges(permissions.map(Permissions::shorthand))
        return claims
    }

    private fun TokenClaims.signClaims(key: HmacKey): String {
        if (DEBUG_MODE) {
            return this.toJson()
        }
        val jws = org.jose4j.jws.JsonWebSignature()
        jws.payload = this.toJson()
        jws.key = key
        jws.algorithmHeaderValue = org.jose4j.jws.AlgorithmIdentifiers.HMAC_SHA512

        return jws.compactSerialization
    }
}
