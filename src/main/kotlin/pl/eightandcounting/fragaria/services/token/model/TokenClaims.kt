package pl.eightandcounting.fragaria.services.token.model

import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.NumericDate

class TokenClaims(private val claims: JwtClaims = JwtClaims()) {
    private val LOGIN = "lgn"
    private val PRIVILEGES = "prv"

    fun setExpirationTime(time: NumericDate) = claims.setExpirationTime(time)
    fun getExpirationTime(): NumericDate = claims.getExpirationTime()
    fun setNotBefore(time: NumericDate) = claims.setNotBefore(time)
    fun setIssuedAt(time: NumericDate) = claims.setIssuedAt(time)

    fun getLogin(): String = claims.getStringClaimValue(LOGIN)

    fun setLogin(login: String) = claims.setClaim(LOGIN, login)

    fun toJson(): String = claims.toJson()

    fun setPrivileges(all: List<String>) = claims.setStringListClaim(PRIVILEGES, all)
    fun getPrivileges(): List<String> = claims.getStringListClaimValue(PRIVILEGES)
}

