package pl.eightandcounting.fragaria.services.token

import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.model.auth.Token
import pl.eightandcounting.fragaria.model.auth.TokenPair

interface TokenBuilderService {
    fun createAccessTokens(login: String, permissions: Set<Permissions>): List<Token>
    fun refreshRefreshToken(login: String, permissions: Set<Permissions>, immediatePermissions: Set<Permissions>): TokenPair
}
