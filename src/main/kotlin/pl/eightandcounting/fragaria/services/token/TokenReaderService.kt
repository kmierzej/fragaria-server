package pl.eightandcounting.fragaria.services.token

import pl.eightandcounting.fragaria.model.auth.TokenCredentials

interface TokenReaderService {
    fun validateAccessToken(token: String): TokenCredentials?
    fun validateRefreshToken(token: String): TokenCredentials?
}
