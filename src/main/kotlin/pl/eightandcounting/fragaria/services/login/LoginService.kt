package pl.eightandcounting.fragaria.services.login

interface LoginService {
    data class PermissionContainer(
        val grantedPermissions: Set<pl.eightandcounting.fragaria.api.Permissions>,
        val allPermissions: Set<pl.eightandcounting.fragaria.api.Permissions>
    )

    fun login(credentials: pl.eightandcounting.fragaria.api.UserCredentials, permissions: Set<pl.eightandcounting.fragaria.api.Permissions>): pl.eightandcounting.fragaria.services.login.LoginService.PermissionContainer?
}
