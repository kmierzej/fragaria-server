package pl.eightandcounting.fragaria.services.login

import org.mindrot.jbcrypt.BCrypt
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.UserCredentials
import pl.eightandcounting.fragaria.repository.userRepository.UserRepository
import pl.eightandcounting.fragaria.services.login.LoginService.PermissionContainer

class LoginServiceImpl( private val userRepository: UserRepository) : LoginService {
    override fun login(credentials: UserCredentials, permissions: Set<Permissions>): PermissionContainer? {
        //verify user credentials
        val user = userRepository.getUserByLogin(credentials.login) ?: return null
        val userPermissions = user.permissions

        val verified = BCrypt.checkpw(credentials.password, user.password)
        if (!verified) {
            return null
        }

        val grantedPermissions = permissions.toMutableSet()
        grantedPermissions.retainAll(userPermissions)

        return PermissionContainer(grantedPermissions = grantedPermissions, allPermissions = userPermissions)
    }
}
