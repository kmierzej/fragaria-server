package pl.eightandcounting.fragaria.services

import com.github.salomonbrys.kodein.*
import org.jose4j.jwt.NumericDate
import pl.eightandcounting.fragaria.services.crypt.*
import pl.eightandcounting.fragaria.services.login.LoginService
import pl.eightandcounting.fragaria.services.login.LoginServiceImpl
import pl.eightandcounting.fragaria.services.project.ProjectAuthenticatorService
import pl.eightandcounting.fragaria.services.project.ProjectAuthenticatorServiceImpl
import pl.eightandcounting.fragaria.services.token.TokenBuilderService
import pl.eightandcounting.fragaria.services.token.TokenBuilderServiceImpl
import pl.eightandcounting.fragaria.services.token.TokenReaderService
import pl.eightandcounting.fragaria.services.token.TokenReaderServiceImpl

val servicesModule = Kodein.Module {
    bind<LoginService>() with provider { LoginServiceImpl(instance()) }
    bind<StringHasher>() with provider { StringHasherImpl() }
    bind<ProjectAuthenticatorService>() with provider { ProjectAuthenticatorServiceImpl(instance()) }

    bind<TokenBuilderService>() with provider { TokenBuilderServiceImpl(instance(), { NumericDate.now() }) }
    bind<TokenReaderService>() with provider { TokenReaderServiceImpl(instance(), { NumericDate.now() }) }

    bind<PasswordEncoder>() with singleton { PasswordEncoderImpl() }
    bind<HmacKeyProvider.HmacPair>() with singleton { HmacKeyProvider(instance("secret")).provide() }
}
