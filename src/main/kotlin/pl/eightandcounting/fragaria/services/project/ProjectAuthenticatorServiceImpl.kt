package pl.eightandcounting.fragaria.services.project

import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectRepository

class ProjectAuthenticatorServiceImpl(private val projectRepository: ProjectRepository): ProjectAuthenticatorService {
    override fun checkVisibility(projectId: String, token: TokenPrincipal) {
        wrapException(token, {projectRepository.visibilityCheck(projectId, token.login)})
    }

    override fun areTasksEditable(projectId: String, token: TokenPrincipal) {
        wrapException(token, {projectRepository.modifyTaskCheck(projectId, token.login)})
    }

    override fun authenticate(projectId: String, token: TokenPrincipal): Unit {
        wrapException(token, {assertIsManager(project = projectId, user = token.login)})
    }

    override fun isProjectLoggable(targetUser: String, projectId: String, tokenPayload: TokenPrincipal) {
        if( tokenPayload.hasActiveAdminPermission()) { return }

        if (targetUser == tokenPayload.login) {
            wrapException(tokenPayload, {projectRepository.logTaskCheck(projectId, targetUser)})
            return
        }

        authenticate(projectId = projectId, token = tokenPayload)
    }

    private inline fun wrapException(token: TokenPrincipal, body: () -> Boolean ) {
        if(token.hasActiveAdminPermission()) { return }
        if(!body.invoke()) {
            // TODO: this probably aint right
            if(token.hasActiveAdminPermission()) {
                throw pl.eightandcounting.fragaria.error.ErrorMessageBasic.LACK_OF_ADMIN_AUTH.toException()
            }
            throw pl.eightandcounting.fragaria.error.ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        }
    }

    override fun assertIsManager(user: String, project: String): Boolean {
        return user == projectRepository.getManagerName(project)
    }
}
