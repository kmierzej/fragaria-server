package pl.eightandcounting.fragaria.services.project

import pl.eightandcounting.fragaria.auth.TokenPrincipal


interface ProjectAuthenticatorService {
    fun authenticate( projectId: String, token: TokenPrincipal)
    fun assertIsManager(user: String, project: String): Boolean

    fun checkVisibility(projectId: String, token: TokenPrincipal)
    fun areTasksEditable(projectId: String, token: TokenPrincipal)
    fun isProjectLoggable(targetUser: String, projectId: String, tokenPayload: TokenPrincipal)
}
