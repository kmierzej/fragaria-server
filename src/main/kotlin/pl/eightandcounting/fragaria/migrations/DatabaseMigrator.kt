package pl.eightandcounting.fragaria.migrations

import com.orientechnologies.orient.core.sql.OCommandSQL
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx
import org.slf4j.LoggerFactory
import pl.eightandcounting.fragaria.repository.DatasetType

class DatabaseMigrator(private val dataset: DatasetType) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun migrateDatabase(factory: OrientGraphFactory): OrientGraphFactory {
        val classloader = this::class.java.classLoader
        val migrations = classloader.getResourceAsStream("database/migrations.sql")

        val graph = factory.noTx
        val startingDBVersion = getDatabaseVersion(graph)
        for ((migrationList, metadata) in MigrationReader(migrations, startingDBVersion)) {
            val (version, type) = metadata
            if(type != dataset && type != DatasetType.INITIAL) {
                continue
            }

            logger.info("Migrating database to : $metadata")
            migrationList.map(::OCommandSQL).forEach { graph.command(it).execute() }

            val isFirst = startingDBVersion == 0 && version == 1
            graph.command(OCommandSQL(getMigrationQuery(isFirst, version))).execute<Any>()
        }

        return factory
    }

    private fun getMigrationQuery(first: Boolean, databaseVersion: Int): String {
       val base = "$databaseVersionString SET version = $databaseVersion"
       return if (first) {
           "CREATE VERTEX $base"
       } else {
           "UPDATE $base"
       }
    }

    private fun getDatabaseVersion(factory: OrientGraphNoTx): Int {
        try {
            val result = factory.getVerticesOfClass(databaseVersionString)
            return result.single().getProperty("version")
        } catch (e : Exception) {
            return 0
        }
    }

    companion object {
        const val databaseVersionString = "DatabaseVersion"
    }
}
