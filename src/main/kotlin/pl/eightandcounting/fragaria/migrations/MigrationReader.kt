package pl.eightandcounting.fragaria.migrations

import org.mindrot.jbcrypt.BCrypt
import pl.eightandcounting.fragaria.repository.DatasetType
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.regex.Pattern

class MigrationReader(migrationStream: InputStream, val currentMigrationNumber: Int): Iterator<MigrationReader.Migration> {
    companion object {
        const val migrationString = "^#(?<version>\\d+(?::\\w+)?)\\s*$"
        const val passwordString = "<password:(?<password>\\w+)>"
    }

    private val reader = BufferedReader(InputStreamReader(migrationStream))
    private var skippedMigrations = false
    private var migrationNumberTemp: MigrationMetadata = MigrationMetadata(0)
    private var released = true

    override fun hasNext(): Boolean {
        skipPreviousMigrations()
        return reader.ready() || !released
    }

    override fun next(): Migration {
        skipPreviousMigrations()
        return getNextMigration()
    }

    private val passwordTransformer = LineTransformer(
        group = "password",
        pattern = passwordString,
        transformation = { password: String -> BCrypt.hashpw(password, BCrypt.gensalt()) }
    )

    private val migrationPattern: Pattern = Pattern.compile(migrationString)

    private var currentLine = 0

    private fun skipPreviousMigrations() {
        if(skippedMigrations) {
            return
        }
        var line: String? = reader.readLine()
        while (line != null) {
            val migrationVersion = getMigrationVersion(line)

            if(migrationVersion != null && migrationVersion.version > currentMigrationNumber) {
                released = false
                migrationNumberTemp = migrationVersion
                currentLine++
                skippedMigrations = true
                return
            }
            currentLine++
            line = reader.readLine()
        }
        skippedMigrations = true
    }

    private fun getNextMigration(): Migration {
        val migrations = mutableListOf<String>()
        var line: String? = reader.readLine()
        if(line == null && released) {
            throw NoSuchElementException()
        }
        while (line != null) {
            val migrationVersion = getMigrationVersion(line)

            if(migrationVersion != null && migrationVersion.version <= migrationNumberTemp.version) {
                currentLine++
                throw Error("Migrations either duplicated or unordered: $currentLine")
            } else if(migrationVersion != null) {
                val swap = migrationNumberTemp
                migrationNumberTemp = migrationVersion
                released = false
                return Migration(migrations, swap)
            } else if (line.isNotBlank()) {
                migrations.add(passwordTransformer.replace(line))
            }
            currentLine++
            line = reader.readLine()
        }

        released = true
        return Migration(migrations, migrationNumberTemp)
    }

    private fun getMigrationVersion(line: String): MigrationMetadata? {
        val matcher = migrationPattern.matcher(line)
        if (matcher.find()) {
            val group = matcher.group("version").split(':')
            val version = group[0].toInt()
            if(group.size > 1) {
                return MigrationMetadata(version, DatasetType.valueOf(group[1]))
            }
            return MigrationMetadata(version)
        }
        return null
    }

    data class Migration(val migrations: List<String>, val metadata: MigrationReader.MigrationMetadata)
    data class MigrationMetadata(val version: Int, val type: DatasetType = DatasetType.INITIAL)
}


