package pl.eightandcounting.fragaria.migrations

import java.util.regex.Matcher
import java.util.regex.Pattern

class LineTransformer(pattern: String,
                      private val group: String,
                      private val transformation: (String) -> String) {
    val pattern: Pattern = Pattern.compile(pattern)

    fun replace(line: String): String {
        val matcher = pattern.matcher(line)
        if(! matcher.find()) return line
        val result = matcher.group(group)
        return matcher.replaceAll(Matcher.quoteReplacement(transformation(result)))
    }
}
