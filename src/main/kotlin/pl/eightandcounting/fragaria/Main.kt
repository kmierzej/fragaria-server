package pl.eightandcounting.fragaria

fun main(args: Array<String>) {
    FragariaApplication().run(*args)
}
