package pl.eightandcounting.fragaria

import org.glassfish.hk2.api.Factory
import javax.inject.Inject
import javax.servlet.http.HttpServletRequest


class RequestAddressFactory @Inject constructor(val request: HttpServletRequest): Factory<RequestAddress> {
    override fun dispose(instance: RequestAddress) { }

    override fun provide(): RequestAddress {
        return RequestAddress(request)
    }
}

class RequestAddress(val remoteAddr: String, val userAgent: String) {
    constructor(request: HttpServletRequest): this(request.remoteAddr, request.getHeader("User-Agent"))
}
