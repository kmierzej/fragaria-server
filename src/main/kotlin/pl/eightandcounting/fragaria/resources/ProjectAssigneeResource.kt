package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import io.dropwizard.auth.Auth
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.repository.projectAssigneesRepository.ProjectAssigneesRepository
import pl.eightandcounting.fragaria.services.project.ProjectAuthenticatorService
import javax.annotation.security.RolesAllowed
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Path("/projects/{pid}/assignee")
class ProjectAssigneeResource(
    private val assigneeRepository: ProjectAssigneesRepository,
    private val projectAuthenticatorService: ProjectAuthenticatorService
) {

    @POST
    @Timed
    @Path("/assignee")
    @RolesAllowed("USER", "ADMIN")
    fun assignUsers(@Auth principal: TokenPrincipal, @PathParam("pid") project: String, body: List<String>): List<UserIdentity> {
        this.projectAuthenticatorService.authenticate(projectId = project, token = principal)

        this.assigneeRepository.addAssigned(body, project)
        return this.assigneeRepository.getAssigned(project)
    }

    @POST
    @Timed
    @Path("/manager")
    @RolesAllowed("USER", "ADMIN")
    fun assignManager(@Auth principal: TokenPrincipal, @PathParam("pid") project: String, body: String): Project {
        this.projectAuthenticatorService.authenticate(projectId = project, token = principal)

        return this.assigneeRepository.setManager(body, project)
    }

    @DELETE
    @Timed
    @Path("/assignee")
    @RolesAllowed("USER", "ADMIN")
    fun removeUsers(@Auth principal: TokenPrincipal, @PathParam("pid") project: String, body: List<String>): List<UserIdentity> {
        this.projectAuthenticatorService.authenticate(projectId = project, token = principal)

        this.assigneeRepository.removeAssigned(body, project)
        return this.assigneeRepository.getAssigned(project)
    }

    @DELETE
    @Timed
    @Path("/manager")
    @RolesAllowed("USER", "ADMIN")
    fun removeManager(@Auth principal: TokenPrincipal, @PathParam("pid") project: String): Project {
        this.projectAuthenticatorService.authenticate(projectId = project, token = principal)

        return this.assigneeRepository.removeManager(project)
    }

    @GET
    @Timed
    @Path("/assignee")
    @RolesAllowed("USER", "ADMIN")
    fun getAssignee(@Auth principal: TokenPrincipal, @PathParam("pid") project: String): List<UserIdentity> {
        this.projectAuthenticatorService.checkVisibility(projectId = project, token = principal)
        return this.assigneeRepository.getAssigned(project)
    }
}
