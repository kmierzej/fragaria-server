package pl.eightandcounting.fragaria.resources

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.eagerSingleton
import com.github.salomonbrys.kodein.instance

val resourcesModule = Kodein.Module {
    bind<LoginResource>() with eagerSingleton { LoginResource(instance(), instance(), instance(), instance(), instance(), instance()) }
    bind<ManageAccountResource>() with eagerSingleton { ManageAccountResource(instance(), instance()) }
    bind<ManageUsersResource>() with eagerSingleton { ManageUsersResource(instance(), instance()) }
    bind<ManageProjectResource>() with eagerSingleton { ManageProjectResource(instance()) }
    bind<ProjectAssigneeResource>() with eagerSingleton { ProjectAssigneeResource(instance(), instance()) }
    bind<ManageTaskResource>() with eagerSingleton { ManageTaskResource(instance(), instance()) }
    bind<TimesheetResource>() with eagerSingleton { TimesheetResource(instance(), instance()) }
}
