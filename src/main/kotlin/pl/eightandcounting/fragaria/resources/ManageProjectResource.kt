package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import io.dropwizard.auth.Auth
import io.dropwizard.jersey.PATCH
import io.dropwizard.jersey.params.IntParam
import pl.eightandcounting.fragaria.api.CreateProject
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.ProjectStatus
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.error.getUnknownProjectException
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectKeys
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectRepository
import pl.eightandcounting.fragaria.repository.transformStatus
import pl.eightandcounting.fragaria.repository.verifyPagingLimit
import pl.eightandcounting.fragaria.repository.withLabel
import java.util.*
import javax.annotation.security.RolesAllowed
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Path("/projects")
class ManageProjectResource(private val projectRepository: ProjectRepository) {

    @PATCH
    @Timed
    @Path("{id}")
    @RolesAllowed("USER", "ADMIN")
    fun patchProject(@PathParam("id") projectId: String, @Valid body: CreateProject, @Auth token: TokenPrincipal): Project {
        if( token.hasActiveAdminPermission() || this.projectRepository.getManagerName(projectId) == token.name) {
            return this.projectRepository.patchProject(projectId, body) ?: throw getUnknownProjectException()
        }
        throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
    }

    @POST
    @Timed
    @RolesAllowed("ADMIN")
    fun createProject(@Valid body: CreateProject): Project {
        return this.projectRepository.createProject(body)
    }

    @DELETE
    @Timed
    @Path("{id}")
    @RolesAllowed("ADMIN")
    fun deleteProject(@Auth principal: TokenPrincipal, @PathParam("id") name: String): Project? {
        return this.projectRepository.deleteProject(name)
    }

    @GET
    @Timed
    @Path("{id}")
    @RolesAllowed("USER", "ADMIN")
    fun getProject(@PathParam("id") name: String, @Auth principal: TokenPrincipal): Project {
        return this.projectRepository.getProjectById(name, principal.login, principal.hasActiveAdminPermission()) ?: throw getUnknownProjectException()
    }

    @GET
    @Timed
    @Path("{id}/canCreateTasks")
    @RolesAllowed("USER", "ADMIN")
    fun canCreateTasks(@PathParam("id") projectId: String, @Auth principal: TokenPrincipal): ProjectStatus {
        if(principal.hasActiveAdminPermission()) {
            return ProjectStatus(true, true, true, true)
        }
        return projectRepository.statusCheck(projectId, principal.login)
    }

    @GET
    @Timed
    @RolesAllowed("USER", "ADMIN")
    fun getProjects(
        @Auth principal: TokenPrincipal,
        @QueryParam("before") before: Optional<String>,
        @QueryParam("from") from: Optional<String>,
        @QueryParam("after") after: Optional<String>,

        @QueryParam("limit") @DefaultValue("10") preVerifyLimit: IntParam,
        @QueryParam("status") receivedStatus: List<Project.Status>
    ): Map<String, Collection<Project>> {
        val status = transformStatus(receivedStatus.map(Project.Status::name), Project.DEFAULT_STATUS)
        val limit = verifyPagingLimit(preVerifyLimit.get())

        val (key, value) = chooseParams(before = before, after = after, from = from, default = "")
        val result = key.callMethod()(this.projectRepository, value, limit, status, principal.login, principal.hasActiveAdminPermission())
        return withLabel("projects", result)
    }

    fun <T>chooseParams(before: Optional<T>, after: Optional<T>, from: Optional<T>, default: T): Pair<ProjectKeys, T> {
        if(before.isPresent) return Pair(ProjectKeys.BEFORE, before.get())
        if(after.isPresent) return Pair(ProjectKeys.AFTER, after.get())
        if(from.isPresent) return Pair(ProjectKeys.FROM, from.get())
        return Pair(ProjectKeys.FROM, default)
    }
}
