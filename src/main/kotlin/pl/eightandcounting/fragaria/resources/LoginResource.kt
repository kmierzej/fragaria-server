package pl.eightandcounting.fragaria.resources

import io.dropwizard.auth.Auth
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty
import pl.eightandcounting.fragaria.RequestAddress
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.TokenApi
import pl.eightandcounting.fragaria.api.UserCredentials
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.model.auth.Token
import pl.eightandcounting.fragaria.model.auth.VerifiedTokenCredentials
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenDB
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenRepository
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenVerifyRepository
import pl.eightandcounting.fragaria.services.crypt.StringHasher
import pl.eightandcounting.fragaria.services.login.LoginService
import pl.eightandcounting.fragaria.services.token.TokenBuilderService
import pl.eightandcounting.fragaria.services.token.TokenReaderService
import javax.annotation.security.RolesAllowed
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType

data class TokenResult(
    val refreshToken: Token?,
    val accessTokens: List<Token>?,
    val allPerm: Collection<Permissions>,
    val login: String
)

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
class LoginResource (
    private val loginService: LoginService,
    private val tokenRepository: TokenRepository,
    private val tokenVerifyRepository: TokenVerifyRepository,
    private val tokenBuilderService: TokenBuilderService,
    private val tokenReaderService: TokenReaderService,
    private val stringHasher: StringHasher
) {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    fun login(
        @Context requestAddress: RequestAddress,
        @Valid @NotNull body: UserCredentials,
        @QueryParam("permissions") requestedPermissions: Set<Permissions>
    ): TokenResult {
        val permissions = loginService.login(
            body,
            if (requestedPermissions.isEmpty()) Permissions.USER_PERMISSION_SET else requestedPermissions
        ) ?: throw ErrorMessageBasic.LOGIN_FAIL_BAD_CREDENTIALS.toException()

        val grantedPermissions = processRequestedPermissions(permissions.grantedPermissions)

        val token = tokenBuilderService.refreshRefreshToken(body.login, grantedPermissions.requested, setOf(grantedPermissions.default))
        val saveSuccess = tokenRepository.saveToken(
            login = body.login,
            token = constructTokenDB(
                token.refreshToken.token,
                token.refreshToken.expirationTime,
                requestAddress,
               true
            )
        )

        if(!saveSuccess) {
            throw ErrorMessageBasic.LOGIN_FAIL_BAD_CREDENTIALS.toException()
        }

        return TokenResult(
            refreshToken = token.refreshToken,
            accessTokens = token.accessTokens,
            allPerm = permissions.allPermissions,
            login = body.login
        )
    }

    data class RefreshUserBody (@NotBlank val token: String)

    @POST
    @Path("/accessTokens")
    @Consumes(MediaType.APPLICATION_JSON)
    fun refreshAccessToken(@Context requestAddress: RequestAddress, @Valid @NotNull body: RefreshUserBody, @QueryParam("permissions") @NotEmpty() requestedPermissions: Set<Permissions>): TokenResult {
        val credentials = tokenReaderService.validateRefreshToken(body.token) ?: throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        val verifiedData = tokenVerifyRepository.verifyToken(credentials, constructTokenDB(body.token, credentials.expirationTime, requestAddress, true)) ?: throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()

        if (verifiedData.permissions != credentials.permissions || verifiedData.login != credentials.login) {
            return shipNewRefreshToken(
                login = verifiedData.login,
                newToken = verifiedData,
                requestedPermissions = requestedPermissions,
                oldToken = Token(body.token, verifiedData.expirationTime, verifiedData.permissions),
                requestAddress = requestAddress
            )
        }
        val newPermissions = requestedPermissions.toMutableSet()
        newPermissions.retainAll(verifiedData.permissions)

        val tokens = tokenBuilderService.createAccessTokens(credentials.login, newPermissions)

        return TokenResult(
            refreshToken = null,
            accessTokens = tokens,
            allPerm = verifiedData.allPermissions,
            login = verifiedData.login
        )
    }

    @POST
    @Path("/refreshToken")
    @Consumes(MediaType.APPLICATION_JSON)
    fun refreshRefreshToken(@Context requestAddress: RequestAddress, @Valid @NotNull body: RefreshUserBody, @QueryParam("permissions") requestedPermissions: Set<Permissions>): TokenResult {
        val credentials = tokenReaderService.validateRefreshToken(body.token) ?: throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        val token = constructTokenDB(body.token, credentials.expirationTime, requestAddress, true)
        val verifiedData = tokenVerifyRepository.verifyToken(credentials, token) ?: throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()

        return shipNewRefreshToken(
            login = verifiedData.login,
            newToken = verifiedData,
            requestedPermissions = requestedPermissions,
            oldToken = Token(body.token, verifiedData.expirationTime, verifiedData.permissions),
            requestAddress = requestAddress
        )
    }

    fun shipNewRefreshToken(
        login: String,
        newToken: VerifiedTokenCredentials,
        requestedPermissions: Set<Permissions>,
        oldToken: Token,
        requestAddress: RequestAddress
    ): TokenResult {
        val newPermissions = requestedPermissions.toMutableSet()
        newPermissions.retainAll(newToken.permissions)

        val token = tokenBuilderService.refreshRefreshToken(newToken.login, newToken.permissions, newPermissions)
        tokenRepository.invalidateAndSaveToken(login, constructTokenDB(oldToken.token, oldToken.expirationTime, requestAddress, false))
        val success = tokenRepository.saveToken(
            login = newToken.login,
            token = constructTokenDB(token.refreshToken.token, token.refreshToken.expirationTime, requestAddress, true)
        )
        if(!success) {
            // that means that user changed login between two user lookups. Therefore token is invalid, need to re-login.
            throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        }

        return TokenResult(
            refreshToken = token.refreshToken,
            accessTokens = token.accessTokens,
            allPerm = newToken.allPermissions,
            login = newToken.login
        )
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    fun logoutAdmin(@Context requestAddress: RequestAddress, @Valid @NotNull body: RefreshUserBody) {
        val credentials = tokenReaderService.validateRefreshToken(body.token) ?: throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        tokenRepository.invalidateAndSaveToken(credentials.login, constructTokenDB(body.token, credentials.expirationTime, requestAddress, false))
    }

    @GET
    @Path("/tokens")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed("USER", "ADMIN")
    fun getTokens(@Auth principal: TokenPrincipal): List<TokenApi>? {
        return tokenRepository.getTokens(principal.login)?.map{TokenApi(it)}
    }

    @DELETE
    @Path("/tokens/{tid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed("USER", "ADMIN")
    fun deleteToken(@Auth principal: TokenPrincipal, @PathParam("tid") tokenId: String): List<TokenApi>? {
        tokenRepository.invalidateToken(principal.login, tokenId)
        return getTokens(principal)
    }

    private fun constructTokenDB(token: String, expTime: Long, requestAddress: RequestAddress, valid: Boolean): TokenDB {
        return TokenDB(
            userAgent = requestAddress.userAgent,
            tokenHash = stringHasher.hashString(token),
            valid = valid,
            lastAccess = System.currentTimeMillis(),
            expTime = expTime,
            source = requestAddress.remoteAddr
        )
    }

}

data class ProcessedPermissions( val default: Permissions, val requested: Set<Permissions> )
fun processRequestedPermissions(permissions: Set<Permissions>): ProcessedPermissions {
    if(permissions.isEmpty()) {
        throw ErrorMessageBasic.DISABLED_ACCOUNT.toException()
    }
    // if contains any higher permission, do only it, and only one of it
    val higherPermission = Permissions.HIGHER_PERMISSIONS.firstOrNull { permissions.contains(it) }
    if (higherPermission != null) {
        return ProcessedPermissions(default = higherPermission, requested = setOf(higherPermission))
    }

    if (permissions.contains(Permissions.USER)) {
        return ProcessedPermissions(default = Permissions.USER, requested = permissions)
    }

    return ProcessedPermissions(default = permissions.first(), requested = permissions)
}

