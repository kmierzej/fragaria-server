package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import io.dropwizard.auth.Auth
import io.dropwizard.jersey.PATCH
import pl.eightandcounting.fragaria.api.CreateUserIdentity
import pl.eightandcounting.fragaria.api.PatchUserIdentity
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.error.getUnknownUserException
import pl.eightandcounting.fragaria.repository.chooseParams
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityParams
import pl.eightandcounting.fragaria.repository.userRepository.UserRepository
import pl.eightandcounting.fragaria.repository.verifyPagingLimit
import pl.eightandcounting.fragaria.repository.withLabel
import pl.eightandcounting.fragaria.services.crypt.PasswordEncoder
import java.util.*
import javax.annotation.security.RolesAllowed
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Produces(MediaType.APPLICATION_JSON)
@Path("/admin")
class ManageUsersResource( private val userRepository: UserRepository,
                           private val passwordEncoder: PasswordEncoder
) {

    @POST
    @Timed
    @Path("/user")
    @RolesAllowed("ADMIN")
    fun createUser(@Valid body: CreateUserIdentity): UserIdentity {
        val vertex = UserIdentityParams(
            login = body.login,
            email = body.email,
            password = passwordEncoder.encode(body.password),
            fullname = body.fullname,
            permissions = body.permissions
        )

        val result = this.userRepository.createUser(vertex)
        return result.toUserIdentity()
    }

    @DELETE
    @Timed
    @Path("/user/{username}")
    @RolesAllowed("ADMIN")
    fun deleteUser(@Auth principal: TokenPrincipal, @PathParam("username") name: String): UserIdentity? {
        if(principal.login == name) {
            throw WebApplicationException(Response.Status.BAD_REQUEST)
        }
        val result = this.userRepository.deleteUser(name) ?: return null
        return result.toUserIdentity()
    }

    @GET
    @Timed
    @Path("/user/{username}")
    @RolesAllowed("USER", "ADMIN")
    fun getUser(@PathParam("username") name: String): UserIdentity {
        val result = this.userRepository.getUserByLogin(name) ?: throw getUnknownUserException()
        return result.toUserIdentity()
    }

    @PATCH
    @Timed
    @Path("/user/{username}")
    @RolesAllowed("ADMIN")
    fun patchUser(@PathParam("username") name: String, body: PatchUserIdentity): UserIdentity {
        fun getPassword(pass: String?): String? {
            return if (pass != null) { passwordEncoder.encode(pass) } else { null }
        }
        val vertex = UserIdentityParams(
            login = body.login ?: name,
            email = body.email,
            password = getPassword(body.password),
            fullname = body.fullname,
            permissions = body.permissions
        )
        val result = this.userRepository.patchUser(vertex, name) ?: throw getUnknownUserException()
        return result.toUserIdentity()
    }

    @GET
    @Timed
    @Path("/user")
    @RolesAllowed("USER", "ADMIN")
    fun getUsers(@QueryParam("before") before: Optional<String>,
                 @QueryParam("from") from: Optional<String>,
                 @QueryParam("after") after: Optional<String>,
                 @QueryParam("limit") @DefaultValue("10") preVerifyLimit: Int,
                 @QueryParam("permission") permission: List<Permissions>
    ): Map<String, Collection<UserIdentity>> {
        // TODO: maybe email should be errased if not admin?

        //val status = transformStatus(permission, arrayOf(Permissions.ADMIN, Permissions.USER))
        val limit = verifyPagingLimit(preVerifyLimit)

        val (key, value) = chooseParams(before = before, after = after, from = from, default = "")
        val result = key.callMethod(this.userRepository, id = value, limit = limit, status = permission.toTypedArray() )
        val mappedResult = result.map { it.toUserIdentity() }
        return withLabel("resources", mappedResult)
    }
}
