package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import io.dropwizard.auth.Auth
import io.dropwizard.jersey.jsr310.LocalDateParam
import org.jetbrains.annotations.NotNull
import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.TimeEntry
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.repository.timesheetRepository.TimesheetRepository
import pl.eightandcounting.fragaria.services.project.ProjectAuthenticatorService
import java.time.LocalDate
import javax.annotation.security.RolesAllowed
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Produces(MediaType.APPLICATION_JSON)
@Path("/timesheet")
class TimesheetResource(
    private val timesheetRepository: TimesheetRepository,
    private val projectAuthenticatorService: ProjectAuthenticatorService
) {
    data class CreateTimeEntry(
        @JsonProperty("date") val date: LocalDate,
        @JsonProperty("user") val user: String,
        @JsonProperty("time") val time: Short)

    @POST
    @Timed
    @Path("/task/{tid}")
    @RolesAllowed("USER", "ADMIN")
    fun createTimeEntry(
        @Auth principal: TokenPrincipal,
        body: CreateTimeEntry,
        @PathParam("tid") taskId: CompositeTaskId
    ) {
        this.projectAuthenticatorService.isProjectLoggable(targetUser = body.user, projectId = taskId.projectId, tokenPayload = principal)
        timesheetRepository.create(taskId = taskId, userId = body.user, date = body.date, time = body.time)
    }


    data class DeleteTimeEntry(
        @JsonProperty("date") val date: LocalDate,
        @JsonProperty("user") val user: String)

    @DELETE
    @Timed
    @Path("/task/{tid}")
    @RolesAllowed("USER", "ADMIN")
    fun deleteTimeEntry(
        @Auth principal: TokenPrincipal,
        body: DeleteTimeEntry,
        @PathParam("tid") taskId: CompositeTaskId
    ) {
        this.projectAuthenticatorService.isProjectLoggable(targetUser = body.user, projectId = taskId.projectId, tokenPayload = principal)
        timesheetRepository.delete(taskId = taskId, userId = body.user, date = body.date)
    }

    @GET
    @Timed
    @Path("/task/{tid}")
    @RolesAllowed("USER", "ADMIN")
    fun getTaskTimesheet(
        @Auth principal: TokenPrincipal,
        @PathParam("tid") taskId: CompositeTaskId,
        @QueryParam("from") from: LocalDateParam,
        @QueryParam("to") to: LocalDateParam
    ): List<TimeEntry> {
        this.projectAuthenticatorService.checkVisibility(projectId = taskId.projectId, token = principal)
        return timesheetRepository.getByTask(taskId = taskId, from = from.get() , to = to.get())
    }

    @GET
    @Timed
    @Path("/project/{pid}")
    @RolesAllowed("USER", "ADMIN")
    fun getProjectTimesheet(
        @Auth principal: TokenPrincipal,
        @PathParam("pid") projectId: String,
        @QueryParam("from") from: LocalDateParam,
        @QueryParam("to") to: LocalDateParam
        ): List<TimeEntry> {
        this.projectAuthenticatorService.checkVisibility(projectId = projectId, token = principal)
        return timesheetRepository.getByProject(projectId = projectId, from = from.get() , to = to.get())
    }

    @GET
    @Timed
    @Path("/user/{uid}")
    @RolesAllowed("USER", "ADMIN")
    fun getUserTimesheet(
        @Auth principal: TokenPrincipal,
        @PathParam("uid") userId: String,
        @QueryParam("from") from: LocalDateParam,
        @QueryParam("to") to: LocalDateParam
    ): List<TimeEntry> {
        authorizeByUser(userId = userId, token = principal)
        return timesheetRepository.getByUser(userId = userId, from = from.get() , to = to.get())
    }

    @GET
    @Timed
    @RolesAllowed("ADMIN")
    fun getAllProjectsTimesheetJSON(
        @QueryParam("from") from: LocalDateParam?,
        @QueryParam("to") to: LocalDateParam?
    ): List<TimeEntry> {
        if (from == null || to == null) {
            throw ErrorMessageBasic.MISSING_PARAMS.toException()
        }
        return timesheetRepository.get(from = from.get() , to = to.get())
    }


    val csvMapper: CsvMapper = CsvMapper()
    val schema: CsvSchema

    init {
        csvMapper.findAndRegisterModules()
        csvMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        schema = csvMapper.schemaFor(TimeEntry::class.java)
    }

    @GET
    @Timed
    @RolesAllowed("ADMIN")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    fun getAllProjectsTimesheetCSV(
        @QueryParam("from") @NotNull from: LocalDateParam?,
        @QueryParam("to") @NotNull to: LocalDateParam?
    ): Response {
        if (from == null || to == null) {
            throw ErrorMessageBasic.MISSING_PARAMS.toException()
        }

        val entries = timesheetRepository.get(from = from.get() , to = to.get())
        val output = csvMapper.writer(schema).writeValueAsBytes(entries)
        return Response.ok(output, MediaType.APPLICATION_OCTET_STREAM)
            .header("Content-Disposition", """attachment; filename="timesheet.csv"""")
            .build()
    }

    fun authorizeByUser(userId: String, token: TokenPrincipal) {
        if(!(userId == token.login || token.hasActiveAdminPermission())) {
            if(token.hasActiveAdminPermission()) {
                throw ErrorMessageBasic.LACK_OF_ADMIN_AUTH.toException()
            }
            throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
        }
    }
}
