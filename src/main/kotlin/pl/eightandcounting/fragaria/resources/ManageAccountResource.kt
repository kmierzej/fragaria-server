package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.auth.Auth
import io.dropwizard.jersey.PATCH
import pl.eightandcounting.fragaria.api.PatchUserIdentity
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.error.getUnknownUserException
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityParams
import pl.eightandcounting.fragaria.repository.userRepository.UserRepository
import pl.eightandcounting.fragaria.services.crypt.PasswordEncoder
import javax.annotation.security.RolesAllowed
import javax.ws.rs.GET
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Path("/account")
class ManageAccountResource(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder
) {
    @GET
    @Timed
    @RolesAllowed("USER")
    fun getAccount(@Auth principal: TokenPrincipal): UserIdentity {
        val result = this.userRepository.getUserByLogin(principal.login) ?: throw getUnknownUserException()
        return result.toUserIdentity()
    }

    @PATCH
    @Timed
    @RolesAllowed("USER")
    @Suppress("unused")
    fun patchUser(@Auth principal: TokenPrincipal, body: PatchUserIdentity): UserIdentity {
        val vertex = UserIdentityParams(
            login = principal.login,
            email = body.email,
            fullname = body.fullname,
            permissions = null,
            password = null
        )

        val result = this.userRepository.patchUser(vertex, principal.login) ?: throw getUnknownUserException()
        return result.toUserIdentity()
    }

    data class PasswordBody(@JsonProperty("oldPassword") val oldPassword: String,
                            @JsonProperty("newPassword") val newPassword: String)

    @PUT
    @Timed
    @Path("/password")
    @RolesAllowed("USER")
    fun changePassword(@Auth principal: TokenPrincipal, body: PasswordBody): UserIdentity {
        val result = userRepository.changePassword(
            login = principal.login,
            oldPassword = body.oldPassword,
            newPassword = passwordEncoder.encode(body.newPassword)
        ) ?: throw getUnknownUserException()
        return result.toUserIdentity()
    }
}
