package pl.eightandcounting.fragaria.resources

import com.codahale.metrics.annotation.Timed
import io.dropwizard.auth.Auth
import io.dropwizard.jersey.PATCH
import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.error.getUnknownTaskException
import pl.eightandcounting.fragaria.auth.TokenPrincipal
import pl.eightandcounting.fragaria.repository.chooseParams
import pl.eightandcounting.fragaria.repository.taskRepository.TaskRepository
import pl.eightandcounting.fragaria.repository.transformStatus
import pl.eightandcounting.fragaria.repository.verifyPagingLimit
import pl.eightandcounting.fragaria.repository.withLabel
import pl.eightandcounting.fragaria.services.project.ProjectAuthenticatorService
import java.util.*
import javax.annotation.security.RolesAllowed
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Produces(MediaType.APPLICATION_JSON)
@Path("/projects/{pr_id}/tasks")
class ManageTaskResource(
    private val taskRepository: TaskRepository,
    private val projectAuthenticatorService: ProjectAuthenticatorService
) {

    @POST
    @Timed
    @RolesAllowed("USER", "ADMIN")
    fun createTask(@Auth principal: TokenPrincipal, body: Task, @PathParam("pr_id") projectId: String): Task? {
        projectAuthenticatorService.areTasksEditable(projectId, principal)
        return this.taskRepository.createTask(copyBody(body), projectId) ?: TODO("NOT IMPLEMENTED")
    }

    @PATCH
    @Timed
    @Path("{id}")
    @RolesAllowed("USER", "ADMIN")
    fun pathTask(
        @Auth principal: TokenPrincipal,
        @PathParam("id") taskId: CompositeTaskId,
        @PathParam("pr_id") projectId: String,
        @Valid body: Task
    ): Task {
        validateTaskId(taskId, projectId)
        projectAuthenticatorService.areTasksEditable(projectId, principal)
        return this.taskRepository.patchTask(copyBody(body), taskId) ?: throw getUnknownTaskException()
    }

    @DELETE
    @Timed
    @Path("{id}")
    @RolesAllowed("USER", "ADMIN")
    fun deleteTask(@Auth principal: TokenPrincipal, @PathParam("pr_id") projectId: String, @PathParam("id") taskId: CompositeTaskId): Task? {
        validateTaskId(taskId, projectId)
        projectAuthenticatorService.authenticate(projectId, principal)
        return this.taskRepository.deleteTask(taskId)
    }

    @GET
    @Timed
    @Path("{id}")
    @RolesAllowed("USER", "ADMIN")
    fun getTask(@Auth principal: TokenPrincipal, @PathParam("pr_id") projectId: String, @PathParam("id") taskId: CompositeTaskId): Task {
        validateTaskId(taskId, projectId)
        projectAuthenticatorService.checkVisibility(projectId, principal)
        return this.taskRepository.getTaskById(taskId) ?: throw getUnknownTaskException()
    }


    @GET
    @Timed
    @RolesAllowed("USER", "ADMIN")
    fun getTasks(
        @Auth principal: TokenPrincipal,
        @QueryParam("before") before: Optional<CompositeTaskId>,
        @QueryParam("from") from: Optional<CompositeTaskId>,
        @QueryParam("after") after: Optional<CompositeTaskId>,

        @QueryParam("limit") @DefaultValue("10") preVerifyLimit: Int,
        @QueryParam("status") receivedStatus: List<Task.Status>,

        @PathParam("pr_id") projectId: String
    ): Map<String, Collection<Task>> {
        projectAuthenticatorService.checkVisibility(projectId, principal)
        //  InjectableProvider
        val limit = verifyPagingLimit(preVerifyLimit)
        val status = transformStatus(receivedStatus, arrayOf(Task.Status.OPEN, Task.Status.PENDING))

        val (key, value) = chooseParams(before = before, after = after, from = from, default = CompositeTaskId(0, projectId))
        validateTaskId(value, projectId)

        val result = key.callMethod(this.taskRepository, id = value, limit = limit, status = status )
        if(result.isEmpty() && receivedStatus.isEmpty()) {
            val retry = key.callMethod(this.taskRepository, id = value, limit = limit, status = arrayOf(Task.Status.CLOSED))
            return withLabel("resources", retry)
        }
        return withLabel("resources", result)
    }

    private fun validateTaskId(taskId: CompositeTaskId, projectId: String) {
        if(!taskId.isValid(projectId)) {
            throw ErrorMessageBasic.INVALID_PATH_PARAMS.toException()
        }
    }

    private fun copyBody(body: Task): Task {
        if(body.id !== null) {
            return body.copy(id = null)
        }
        return body
    }

}
