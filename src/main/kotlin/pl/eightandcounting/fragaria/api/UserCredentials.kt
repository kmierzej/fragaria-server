package pl.eightandcounting.fragaria.api

import org.hibernate.validator.constraints.NotBlank

data class UserCredentials (
    @NotBlank val login: String,
    @NotBlank val password: String
)
