package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.annotation.JsonProperty

data class ProjectStatus(
    @JsonProperty("visible") val visible: Boolean,
    @JsonProperty("editable") val editable: Boolean,
    @JsonProperty("loggable") val loggable: Boolean,
    @JsonProperty("isManager") val isManager: Boolean
)
