package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.validation.OneOf

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Project(
    @JsonProperty(ID) val id: String?,
    @JsonProperty(TITLE) val title: String?,
    @JsonProperty(DESCRIPTION) val description: String?,
    @JsonProperty(STATUS) @field:OneOf("OPEN", "CLOSED", "PENDING") val status: String?,

    @JsonProperty(LOGGING_WHO_STRATEGY) @field:OneOf("ASSIGNED", "ALL") val loggingWhoStrategy: String?,
    @JsonProperty(LOGGING_WHEN_STRATEGY) @field:OneOf("ANY", "WEEKLY", "MONTHLY", "WEEKLY_MONTHLY") val loggingWhenStrategy: String?,
    @JsonProperty(CREATE_TASK_STRATEGY) @field:OneOf("ASSIGNED", "MANAGER", "ALL") val createTaskStrategy: String?,
    @JsonProperty(VISIBILITY_STRATEGY) @field:OneOf("ALL", "ASSIGNED") val visibilityStrategy: String?,

    val totalLogged: Int?,
    val estimatedTime: Int?,

    @JsonProperty(MANAGER) val manager: String?
) {
    enum class Status { OPEN, CLOSED, PENDING }
    enum class LoggingWhoStrategy { ALL, ASSIGNED }
    enum class LoggingWhenStrategy { ANY, WEEKLY, MONTHLY, WEEKLY_MONTHLY }
    enum class CreateTaskStrategy { MANAGER, ASSIGNED, ALL }
    enum class VisibilityStrategy { ALL, ASSIGNED }

    companion object {
        val DEFAULT_STATUS = arrayOf("OPEN", "PENDING");

        const val CLASS = "Project"
        const val ID = "id"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val STATUS = "status"

        const val LOGGING_WHO_STRATEGY = "loggingWhoStrategy"
        const val LOGGING_WHEN_STRATEGY = "loggingWhenStrategy"
        const val CREATE_TASK_STRATEGY = "createTaskStrategy"
        const val VISIBILITY_STRATEGY = "visibilityStrategy"

        const val MANAGER = "manager"
        const val MANAGER_EDGE = "hasManager"
        const val ASSIGNEE_EDGE = "hasAssignee"

        const val ASSIGN_EDGE = "assignEdge"

        const val TASK_EDGE = "hasTask"
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class CreateProject(
    val id: String?,
    val title: String?,
    val description: String?,
    @field:OneOf("OPEN", "CLOSED", "PENDING") val status: String?,

    @field:OneOf("ASSIGNED", "ALL") val loggingWhoStrategy: String?,
    @field:OneOf("ANY", "WEEKLY", "MONTHLY", "WEEKLY_MONTHLY") val loggingWhenStrategy: String?,
    @field:OneOf("ASSIGNED", "MANAGER", "ALL") val createTaskStrategy: String?,
    @field:OneOf("ALL", "ASSIGNED") val visibilityStrategy: String?,
    val estimatedTime: Int?,
    val manager: String?,
    val assignees: List<String>?
)
