package pl.eightandcounting.fragaria.api

import `is`.tagomor.woothee.Classifier
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenDB

data class TokenApi (
    val userAgent: Map<String, String>,
    val tokenHash: String,
    val lastAccess: Long,
    val expTime: Long,
    val source: String
){
    constructor(token: TokenDB): this(
        userAgent = Classifier.parse(token.userAgent),
        tokenHash = token.tokenHash,
        lastAccess = token.lastAccess,
        expTime = token.expTime,
        source = token.source
    )
}
