package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

data class TimeEntry (
    @JsonProperty("user") val user: String,
    @JsonProperty("task") val task: String,

    @JsonProperty("date") val date: LocalDate,
    @JsonProperty("time") val time: Short
) {
    companion object{
        const val CLASS = "workedOn"
        const val DATE = "date"
        const val TIME = "time"
    }
}
