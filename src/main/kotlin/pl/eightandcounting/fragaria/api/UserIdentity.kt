package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class CreateUserIdentity(
    @NotBlank val login: String,
    @NotBlank val email: String,
    @NotBlank val password: String,
    @NotNull val permissions: Set<Permissions>,
    val fullname: String?
)

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class PatchUserIdentity(
    val login: String?,
    val email: String?,
    val password: String?,
    val permissions: Set<Permissions>?,
    val fullname: String?
)

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class UserIdentity(
    val login: String?,
    val email: String?,
    val fullname: String?,
    val permissions: Set<Permissions>?
)
