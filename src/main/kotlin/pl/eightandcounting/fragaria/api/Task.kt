package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.util.*
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Task(
    val id: String?,

    val title: String?,
    val description: String?,
    val status: Status?,

    val totalLogged: Int?,
    val estimatedTime: Int?,

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd HH:mm z") val created: Date?,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd HH:mm z") val updated: Date?
) {
    enum class Status {
        OPEN,
        CLOSED,
        PENDING,
    }

    companion object {
        const val CLASS = "Task"
        const val SEQ = "seq"
        const val PROJECT_ID = "projectId"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val STATUS = "status"

        const val CREATED = "created"
        const val UPDATED = "updated"
    }
    @JsonIgnore
    fun getParams(): HashMap<String, Any> {
        val map = HashMap<String, Any>()
     //  if(id !== null) {
     //      val splitId = CompositeTaskId.splitId(id);
     //      map.put(PROJECT_ID, splitId.projectId)
     //      map.put(SEQ, splitId.id)
     //  }
        map.putExisting(TITLE, title)
        map.putExisting(DESCRIPTION, description)
        map.putExisting(STATUS, status)
        map.putExisting("estimatedTime", estimatedTime)

        return map
    }

    private fun HashMap<String, Any>.putExisting(key: String, value: Any?) {
        if(value != null) this.put(key, value)
    }
}

data class CompositeTaskId(val id: Short, val projectId: String) {
    fun isValid(newProjectId: String): Boolean {
        return projectId == newProjectId
    }

    companion object {
        @JvmStatic
        @Suppress("unused")
        fun fromString(id: String): CompositeTaskId{
            val getException = { WebApplicationException("Parametr niezgodny z formatem: <string>-<numer>", Response.Status.BAD_REQUEST)}

            val index = id.lastIndexOf('-')
            if (index < 0) { throw getException() }
            val newProjectId = id.substring(0, index)
            val newId = id.substring(index + 1)
            if (newProjectId.isEmpty()) { throw getException() }
            try {
                return CompositeTaskId(newId.toShort(), newProjectId)
            } catch( e: NumberFormatException) {
                throw getException()
            }
        }
    }

    override fun toString(): String = "$projectId-$id"
}
