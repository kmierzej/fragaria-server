package pl.eightandcounting.fragaria.api

import java.util.*

enum class Permissions(val shorthand: String) {
    ADMIN("A"), USER("U"), ACCOUNTANT("C");

    companion object {
        val USER_PERMISSION_SET: Set<Permissions> = getUnmodifiableSet(USER)
        val HIGHER_PERMISSIONS: Set<Permissions> = getUnmodifiableSet(ADMIN)
        val LOWER_PERMISSIONS: Set<Permissions> = getUnmodifiableSet(USER, ACCOUNTANT)

        fun getPermission(value: String): Permissions? {
            return when(value) {
                "A" -> Permissions.ADMIN
                "U" -> Permissions.USER
                "C" -> Permissions.ACCOUNTANT
                else -> null
            }
        }

        private fun getUnmodifiableSet(vararg permissions: Permissions): Set<Permissions> {
            val set = EnumSet.noneOf(Permissions::class.java)
            set.addAll(permissions)
            return Collections.unmodifiableSet(set)
        }
    }
}
