package pl.eightandcounting.fragaria.jobs

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.provider
import org.knowm.sundial.SundialJobScheduler
import org.knowm.sundial.annotations.SimpleTrigger
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenRepository
import java.util.concurrent.TimeUnit

@SimpleTrigger(repeatInterval = 30, timeUnit = TimeUnit.MINUTES)
class CleanTokensJob: org.knowm.sundial.Job() {
    private val db: TokenRepository

    override fun doRun() {
        db.cleanTokens()
    }

    init {
        val injector = SundialJobScheduler.getServletContext().getAttribute("INJECTOR") as Kodein
        db = injector.provider<TokenRepository>().invoke()
    }
}
