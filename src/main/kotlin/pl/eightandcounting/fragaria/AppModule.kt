package pl.eightandcounting.fragaria

import com.github.salomonbrys.kodein.Kodein
import pl.eightandcounting.fragaria.auth.authModule
import pl.eightandcounting.fragaria.repository.repositoryModule
import pl.eightandcounting.fragaria.resources.resourcesModule
import pl.eightandcounting.fragaria.services.servicesModule

val appModule = Kodein.Module {
    import(repositoryModule)
    import(servicesModule)
    import(authModule)
    import(resourcesModule)
}
