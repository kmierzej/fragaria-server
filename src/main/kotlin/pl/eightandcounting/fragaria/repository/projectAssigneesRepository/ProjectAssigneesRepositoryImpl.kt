package pl.eightandcounting.fragaria.repository.projectAssigneesRepository

import com.orientechnologies.orient.core.storage.ORecordDuplicatedException
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.error.getUnknownProjectException
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectVertex
import pl.eightandcounting.fragaria.repository.query.QueryWrapper
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityGraph
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityVertexManipulator

class ProjectAssigneesRepositoryImpl(private val database: OrientGraphFactory): ProjectAssigneesRepository {
    override fun setManager(manager: String, projectId: String): Project {
        return autoclose(database.noTx) { graph ->
            val project = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(projectId)).firstOrNull() as OrientVertex? ?: throw getUnknownProjectException()
            project.getEdges(Direction.OUT, Project.MANAGER_EDGE).forEach(Edge::remove)

            val uGraph = UserIdentityGraph(graph)
            val managerVertex = uGraph.findByLogin(arrayOf(manager))
            for(user in managerVertex) {
                 project.addEdge(Project.MANAGER_EDGE, user.vertex)
            }
            return@autoclose ProjectVertex(project).getCanonicalIdentity()
        }
    }

    override fun addAssigned(users: List<String>, projectId: String) {
        if(users.isEmpty()) { return }
        return autoclose(database.noTx) { graph ->
            val project = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(projectId)).firstOrNull() ?: throw getUnknownProjectException()

            val uGraph = UserIdentityGraph(graph)
            val usersVertices = uGraph.findByLogin(users.toTypedArray())
            for(user in usersVertices) {
                try {
                    project.addEdge(Project.ASSIGNEE_EDGE, user.vertex)
                } catch ( e: ORecordDuplicatedException) {
                    // skip duplicated exceptions
                }
            }
        }
    }

    override fun removeManager(projectId: String): Project {
        return autoclose(database.tx) { graph ->
            val project = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(projectId)).firstOrNull() as OrientVertex? ?: throw getUnknownProjectException()
            project.getEdges(Direction.OUT, Project.MANAGER_EDGE).forEach(Edge::remove)
            return@autoclose ProjectVertex(project).getCanonicalIdentity()
        }
    }

    override fun removeAssigned(users: List<String>, projectId: String) {
        autoclose(database.tx) { graph ->
            val project = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(projectId)).firstOrNull() ?: throw getUnknownProjectException()

            val query = "DELETE EDGE ${Project.ASSIGNEE_EDGE} FROM :project TO :user"

            val uGraph = UserIdentityGraph(graph)
            val usersVertices = uGraph.findByLogin(users.toTypedArray())
            for(user in usersVertices) {
                val params = HashMap<String, Any>()
                params.put("project", project)
                params.put("user", user.vertex)

                val wrapper = QueryWrapper(query, params)
                graph.execute(wrapper)
            }
        }
    }

    override fun getAssigned(projectId: String): List<UserIdentity> {
        autoclose(database.noTx) { graph ->
            val project = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(projectId)).firstOrNull() ?: throw getUnknownProjectException()
            val users = project.getEdges(Direction.OUT, Project.ASSIGNEE_EDGE).map { it.getVertex(Direction.IN) as OrientVertex }.map { UserIdentityVertexManipulator(it) }

            return users.map { user ->
                return@map user.toDao().toUserIdentity()
            }
        }
    }
}
