package pl.eightandcounting.fragaria.repository.projectAssigneesRepository

import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.repository.Repository


interface ProjectAssigneesRepository: Repository {
    fun addAssigned(users: List<String>, projectId: String)
    fun removeAssigned(users: List<String>, projectId: String)
    fun getAssigned(projectId: String): List<UserIdentity>
    fun setManager(manager: String, projectId: String): Project
    fun removeManager(projectId: String): Project
}
