package pl.eightandcounting.fragaria.repository.projectRepository

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.raymanrt.orientqb.query.Clause
import com.github.raymanrt.orientqb.query.Parameter.parameter
import com.github.raymanrt.orientqb.query.Projection.projection
import com.github.raymanrt.orientqb.query.ProjectionFunction.out
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph
import com.tinkerpop.blueprints.impls.orient.OrientEdge
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import pl.eightandcounting.fragaria.api.CreateProject
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.ProjectStatus
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.error.getMissingParamException
import pl.eightandcounting.fragaria.error.getUnknownProjectException
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityGraph
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityVertexManipulator
import java.util.*

class ProjectRepositoryImpl (private val database: OrientGraphFactory, private val mapper: ObjectMapper) : ProjectRepository {
    val queryBuilder = PagedResourceQueryBuilder(
        key = Project.ID,
        clazz = Project.CLASS,
        projections = arrayOf("*")
    )

    override fun getManagerName(projectId: String): String? {
        return autoclose(database.noTx) { graph ->
            val vertex = graph.getVertices(Project.ID, projectId).firstOrNull() as OrientVertex? ?: throw getUnknownProjectException()
            return@autoclose ProjectVertex(vertex).getManagerLogin()
        }
    }

    override fun deleteProject(id: String): Project? {
        return autoclose(database.tx) { graph ->
            val vertex = graph.getVertices(Project.CLASS, arrayOf(Project.ID), arrayOf(id)).firstOrNull() as OrientVertex? ?: return null
            graph.removeVertex(vertex)

            return@autoclose ProjectVertex(vertex).getCanonicalIdentity()
        }
    }

    override fun createProject(project: CreateProject): Project {
        if(project.id == null || project.id.isBlank()) {
            throw getMissingParamException(Project.ID)
        }

        return autoclose(database.tx) { graph ->
            graph.isStandardElementConstraints = false
            val vertex = graph.findVertices(queryBuilder.getQuery(project.id)).map(::ProjectVertex).firstOrNull()

            if (vertex != null) {
                val params = mapper.getParamsMap(project)
                params.remove("totalLogged")
                patchProject(vertex, params)
                return@autoclose vertex.getCanonicalIdentity()
            }

            return@autoclose createProjectWhenNotExists(project, graph)
        }
    }

    private fun createProjectWhenNotExists(project: CreateProject, graph: OrientBaseGraph): Project {
        if(project.title == null || project.title.isBlank()) {
            throw getMissingParamException(Project.TITLE)
        }

        val params = mapper.getParamsMap(project).toMutableMap()

        // must be before create project
        val manager = params.remove(Project.MANAGER) as String?
        val assignees = params.remove("assignees") as List<String>?

        val vertex = ProjectVertex(graph.addVertex("class:${Project.CLASS}", params))
        if (manager != null) {
            setManager(manager, vertex.vertex, graph)
        }
        if (assignees != null && assignees.isNotEmpty()) {
            setAssigned(assignees.toMutableList(), vertex.vertex, graph)
        }

        return vertex.getCanonicalIdentity()
    }

    override fun patchProject(projectId: String, project: CreateProject): Project? {
        if(project.id != null && project.id.isBlank()) {
            throw getMissingParamException(Project.ID)
        }

        return autoclose(database.tx) { graph ->
            val vertex = graph.findVertices(queryBuilder.getQuery(projectId)).map(::ProjectVertex).firstOrNull() ?: return null

            if(project.id !== null) {
                graph.isStandardElementConstraints = false
                vertex.renameTasksIdTo(project.id)
            }

            val params = mapper.getParamsMap(project).toMutableMap()

            val manager = params.remove(Project.MANAGER) as String?
            if (manager != null) {
                setManager(manager, vertex.vertex, graph)
            }

            val assignees = params.remove("assignees") as List<String>?
            if (assignees != null) {
                setAssigned(assignees.toMutableList(), vertex.vertex, graph)
            }

            patchProject(vertex, params)

            return@autoclose vertex.getCanonicalIdentity()
        }
    }

    private fun setManager(manager: String, project: Vertex, graph: OrientBaseGraph) {
        project.getEdges(Direction.OUT, Project.MANAGER_EDGE).forEach(Edge::remove)

        val uGraph = UserIdentityGraph(graph)
        val user = uGraph.findByLogin(manager) ?: return
        project.addEdge(Project.MANAGER_EDGE, user.vertex)
    }

    fun setAssigned(users: MutableList<String>, project: OrientVertex, graph: OrientBaseGraph) {
        val checked = mutableListOf<String>()
        val edges = project.getEdges(Direction.OUT, Project.ASSIGNEE_EDGE) as Iterable<OrientEdge>

        for(edge in edges) {
            val userVertex = UserIdentityVertexManipulator(edge.getVertex(Direction.IN))
            val login = userVertex.getLogin()
            if(users.contains(login)) {
                checked.add(login)
            } else {
                edge.remove()
            }
        }

        users.removeAll(checked)

        val uGraph = UserIdentityGraph(graph)
        val usersVertices = uGraph.findByLogin(users.toTypedArray())

        // val usersVertices = graph.findVertices(getUsersQuery(users))
        usersVertices.forEach { user ->
            try {
                project.addEdge(Project.ASSIGNEE_EDGE, user.vertex)
            } catch ( e: ORecordDuplicatedException) {
                // skip duplicated exceptions
            }
        }
    }

    /*
    private fun getUsersQuery(users: List<String>): QueryWrapper {
        val query = "SELECT * FROM ${UserIdentity.CLASS} WHERE login in :login"
        val params = HashMap<String, Any>(2)
        params.put("login", users.toTypedArray())
        return QueryWrapper(query, params)
    }
    */

    private fun patchProject(vertex: ProjectVertex, params: Map<String, Any>) {
        params.forEach {key, value -> vertex.vertex.setProperty(key, value)}
    }

    override fun getProjectById(projectId: String, login: String, isActiveAdmin: Boolean): Project? {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(projectId)
            val vertex = graph.findVertices(query).map(::ProjectVertex).firstOrNull() ?: return null

            if (!isActiveAdmin) {
                if(!vertex.isVisibleFor(login)) {
                    throw ErrorMessageBasic.FORBIDDEN_RESOURCE.toException()
                }
            }
            return@autoclose vertex.getProjectedIdentity()
        }
    }

    override fun visibilityCheck(projectId: String, login: String): Boolean {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(projectId)
            val vertex = graph.findVertices(query).map(::ProjectVertex).firstOrNull() ?: throw getUnknownProjectException()

            return@autoclose vertex.isVisibleFor(login)
        }
    }

    override fun modifyTaskCheck(projectId: String, login: String): Boolean {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(projectId)
            val vertex = graph.findVertices(query).map(::ProjectVertex).firstOrNull() ?: throw getUnknownProjectException()

            return@autoclose vertex.isVisibleFor(login) && vertex.canCreateTask(login)
        }
    }

    override fun logTaskCheck(projectId: String, login: String): Boolean {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(projectId)
            val vertex = graph.findVertices(query).map(::ProjectVertex).firstOrNull() ?: throw getUnknownProjectException()

            return@autoclose vertex.isVisibleFor(login) && vertex.canLogTask(login)
        }
    }

    override fun statusCheck(projectId: String, login: String): ProjectStatus {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(projectId)
            val vertex = graph.findVertices(query).map(::ProjectVertex).firstOrNull() ?: throw getUnknownProjectException()

            val visibility = vertex.isVisibleFor(login)
            if (!visibility) {
                return@autoclose ProjectStatus(visible = false, editable = false, loggable = false, isManager = false)
            }
            return@autoclose ProjectStatus(
                visible = visibility,
                editable = vertex.canCreateTask(login),
                loggable = vertex.canLogTask(login),
                isManager = vertex.isManager(login)
            )
        }
    }

    override fun getBeforeId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project> {
        return autoclose(database.noTx) { graph ->
            val beforeQuery = queryBuilder.getBeforeQuery(id, limit, getFilter(isActiveAdmin, login, status))
            val result = graph.findVertices(beforeQuery).map(::ProjectVertex).reversed().toMutableList()
            if(result.size < limit) {
                val afterQuery = queryBuilder.getAfterQuery(id, limit - result.size, getFilter(isActiveAdmin, login, status), inclusive = true)
                val result2 = graph.findVertices(afterQuery).map(::ProjectVertex)
                result.addAll(result2)
            }
            return@autoclose result.map { it.getProjectedIdentity()}
        }
    }

    override fun getFromId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project> {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getFromQuery(id, limit, getFilter(isActiveAdmin, login, status))
            val result = graph.findVertices(query).map(::ProjectVertex).map { it.getProjectedIdentity()}
            return@autoclose result
        }
    }

    override fun getAfterId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project> {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getAfterQuery(id, limit, getFilter(isActiveAdmin, login, status))
            return@autoclose graph.findVertices(query).map(::ProjectVertex).map { it.getProjectedIdentity()}
        }
    }

    private fun getFilter(isActiveAdmin: Boolean, login: String, status: Array<String>): PagedResourceQueryBuilder.Filter? {
        val statusFilter = getStatusFilter(status)
        if(isActiveAdmin) {
            return statusFilter
        }

        val visibilityFilter = getVisibilityFilter(login)
        return PagedResourceQueryBuilder.Filter.addFilters(visibilityFilter, statusFilter)
    }

    private fun getVisibilityFilter(login: String): PagedResourceQueryBuilder.Filter {
        val paramName = "login"
        val isAssigneeOrManager = out(Project.ASSIGN_EDGE).dot(projection(UserIdentityVertexManipulator.LOGIN)).contains(parameter(paramName))
        val isProjectPublic = projection(Project.VISIBILITY_STRATEGY).eq("ALL")

        val filter = Clause.or(isAssigneeOrManager, isProjectPublic)
        val params = Collections.singletonMap(paramName, login)
        return PagedResourceQueryBuilder.Filter(filter, params)
    }

    private fun getStatusFilter(status: Array<String>): PagedResourceQueryBuilder.Filter? {
        if(status.isEmpty()) {
            return null
        }
        return PagedResourceQueryBuilder.Filter.simpleInClause(Project.STATUS, status)
    }
}
