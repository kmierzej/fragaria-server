package pl.eightandcounting.fragaria.repository.projectRepository

import pl.eightandcounting.fragaria.api.CreateProject
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.ProjectStatus
import pl.eightandcounting.fragaria.repository.Repository
import kotlin.reflect.KFunction6

interface ProjectRepository : Repository {
    fun getProjectById(projectId: String, login: String, isActiveAdmin: Boolean): Project?

    fun createProject(project: CreateProject): Project
    fun patchProject(projectId: String, project: CreateProject): Project?
    fun deleteProject(id: String): Project?
    fun getFromId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project>
    fun getBeforeId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project>
    fun getAfterId(id: String, limit: Int, status: Array<String>, login: String, isActiveAdmin: Boolean): Collection<Project>

    fun getManagerName(projectId: String): String?
    fun visibilityCheck(projectId: String, login: String): Boolean
    fun modifyTaskCheck(projectId: String, login: String): Boolean
    fun logTaskCheck(projectId: String, login: String): Boolean
    fun statusCheck(projectId: String, login: String): ProjectStatus
}

enum class ProjectKeys {
    BEFORE {
        override fun callMethod(): KFunction6<ProjectRepository, @ParameterName(name = "id") String, @ParameterName(name = "limit") Int, @ParameterName(name = "status") Array<String>, @ParameterName(name = "login") String, @ParameterName(name = "isActiveAdmin") Boolean, Collection<Project>> {
            return ProjectRepository::getBeforeId
        }
    },
    AFTER {
        override fun callMethod(): KFunction6<ProjectRepository, @ParameterName(name = "id") String, @ParameterName(name = "limit") Int, @ParameterName(name = "status") Array<String>, @ParameterName(name = "login") String, @ParameterName(name = "isActiveAdmin") Boolean, Collection<Project>> {
            return ProjectRepository::getAfterId
        }
    },
    FROM {
        override fun callMethod(): KFunction6<ProjectRepository, @ParameterName(name = "id") String, @ParameterName(name = "limit") Int, @ParameterName(name = "status") Array<String>, @ParameterName(name = "login") String, @ParameterName(name = "isActiveAdmin") Boolean, Collection<Project>> {
            return ProjectRepository::getFromId
        }
    };

    abstract  fun callMethod(): KFunction6<ProjectRepository, @ParameterName(name = "id") String, @ParameterName(name = "limit") Int, @ParameterName(name = "status") Array<String>, @ParameterName(name = "login") String, @ParameterName(name = "isActiveAdmin") Boolean, Collection<Project>>
}
