package pl.eightandcounting.fragaria.repository.projectRepository

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import pl.eightandcounting.fragaria.api.Project
import pl.eightandcounting.fragaria.api.Project.*
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.repository.get
import pl.eightandcounting.fragaria.repository.getOptional
import pl.eightandcounting.fragaria.repository.getString
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityVertexManipulator


class ProjectVertex(val vertex: OrientVertex) {
    fun getCanonicalIdentity(): Project {
        return getIdentity()
    }

    fun getProjectedIdentity(): Project {
        return getIdentity()
    }

    private fun getIdentity(): Project {
        return Project(
            id = vertex.get(Project.ID),
            description = vertex.getOptional(Project.DESCRIPTION),
            title = vertex.get(Project.TITLE),
            status =  vertex.get(Project.STATUS), //vertex.getEnum(Project.STATUS, Project.Status.CLOSED),

            loggingWhoStrategy = vertex.get(Project.LOGGING_WHO_STRATEGY),
            loggingWhenStrategy = vertex.get(Project.LOGGING_WHEN_STRATEGY),
            visibilityStrategy = vertex.get(Project.VISIBILITY_STRATEGY),
            createTaskStrategy = vertex.get(Project.CREATE_TASK_STRATEGY),

            estimatedTime = vertex.get("estimatedTime"),
            totalLogged = vertex.get("totalLogged"),

            manager = getManagerLogin()
        )
    }

    fun getManagerLogin(): String? {
        return vertex.getEdges(Direction.OUT, Project.MANAGER_EDGE).map {
            val vertex = UserIdentityVertexManipulator(it.getVertex(Direction.IN) as OrientVertex)
            return@map vertex.getLogin()
        }.firstOrNull()
    }

    fun renameTasksIdTo(name: String) {
        vertex.getEdges(Direction.OUT, Project.TASK_EDGE).asSequence().map {
            return@map it.getVertex(Direction.IN)
        }.forEach {
            it.setProperty(Task.PROJECT_ID, name)
        }
    }

    fun isManager(name: String): Boolean {
        return getManagerLogin() == name
    }

    fun isAssigneeOrManager(name: String): Boolean {
        return vertex.getEdges(Direction.OUT, Project.ASSIGN_EDGE).indexOfFirst {
            val vertex = UserIdentityVertexManipulator(it.getVertex(Direction.IN) as OrientVertex)
            return@indexOfFirst vertex.getLogin() == name
        } > -1
    }

    fun isVisibleFor(name: String): Boolean {
        val isVisibilityStrategyAll = vertex.getString(Project.VISIBILITY_STRATEGY) == VisibilityStrategy.ALL.name
        return isVisibilityStrategyAll || isAssigneeOrManager(name)
    }

    fun canCreateTask(name: String): Boolean {
        val createStrategy = vertex.getString(Project.CREATE_TASK_STRATEGY)

        return (createStrategy == CreateTaskStrategy.ALL.name)
            || (createStrategy == CreateTaskStrategy.MANAGER.name && isManager(name))
            || (createStrategy == CreateTaskStrategy.ASSIGNED.name && isAssigneeOrManager(name))
    }

    fun canLogTask(name: String): Boolean {
        val loggingStrategy = vertex.getString(Project.LOGGING_WHO_STRATEGY)

        return (loggingStrategy == LoggingWhoStrategy.ALL.name)
            || (loggingStrategy == LoggingWhoStrategy.ASSIGNED.name && isAssigneeOrManager(name) )
    }
}
