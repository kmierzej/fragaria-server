package pl.eightandcounting.fragaria.repository.userRepository

import com.github.raymanrt.orientqb.query.Parameter.parameter
import com.github.raymanrt.orientqb.query.Projection.projection
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.error.getMissingParamException
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder
import java.util.*

class UserRepositoryImpl (private val database: OrientGraphFactory) : UserRepository {
    val queryBuilder = PagedResourceQueryBuilder(
        key = UserIdentityVertexManipulator.LOGIN,
        clazz = UserIdentityVertexManipulator.CLASS,
        projections = arrayOf("*")
    )

    override fun changePassword(login: String, oldPassword: String, newPassword: String): UserIdentityVertex? {
        return autoclose(database.noTx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val vertex = nGraph.findByLogin(login) ?: return@autoclose null

            vertex.setPassword(oldPassword = oldPassword, newPassword = newPassword)

            return@autoclose vertex.toDao()
        }
    }

    override fun deleteUser(login: String): UserIdentityVertex? {
        return autoclose(database.tx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val vertex = nGraph.findByLogin(login) ?: return@autoclose null

            val result = vertex.toDao()
            nGraph.deleteUser(vertex)

            return@autoclose result
        }
    }

    override fun createUser(user: UserIdentityParams): UserIdentityVertex {
        return autoclose(database.tx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val vertex = nGraph.findByLogin(user.login)

            if(vertex != null) {
                vertex.setProperties(user)
                return@autoclose vertex.toDao()
            }

            return@autoclose createUserWhenNotExists(user, nGraph).toDao()
        }
    }

    private fun createUserWhenNotExists(user: UserIdentityParams, graph: UserIdentityGraph): UserIdentityVertexManipulator {
        if(user.password == null || user.password.isBlank()) {
            throw getMissingParamException("password")
        }

        return graph.createUser(user)
    }

    override fun patchUser(user: UserIdentityParams, target: String): UserIdentityVertex? {
        return autoclose(database.tx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val vertex = nGraph.findByLogin(target)?: return@autoclose null

            vertex.setProperties(user)

            return@autoclose vertex.toDao()
        }
    }

    override fun getUserByLogin(login: String): UserIdentityVertex? {
        return autoclose(database.noTx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val result = nGraph.findByLogin(login) ?: return@autoclose null

            return@autoclose result.toDao()
        }
    }

    override fun getBeforeId(id: String, limit: Int, status: Array<Permissions>): Collection<UserIdentityVertex> {
        return autoclose(database.noTx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val beforeQuery = queryBuilder.getBeforeQuery(id, limit, getFilter(status))
            val result = nGraph.findVertices(beforeQuery).reversed().toMutableList()
            if(result.size < limit) {
                val afterQuery = queryBuilder.getAfterQuery(id, limit - result.size, getFilter(status), inclusive = true)
                val result2 = nGraph.findVertices(afterQuery)
                result.addAll(result2)
            }
            return@autoclose result.map(UserIdentityVertexManipulator::toDao)
        }
    }

    override fun getFromId(id: String, limit: Int, status: Array<Permissions>): Collection<UserIdentityVertex> {
        return autoclose(database.noTx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val query = queryBuilder.getFromQuery(id, limit, getFilter(status))
            val result = nGraph.findVertices(query)
            return@autoclose result.map(UserIdentityVertexManipulator::toDao)
        }
    }

    override fun getAfterId(id: String, limit: Int, status: Array<Permissions>): Collection<UserIdentityVertex> {
        return autoclose(database.noTx) { graph ->
            val nGraph = UserIdentityGraph(graph)
            val query = queryBuilder.getAfterQuery(id, limit, getFilter(status))
            val result = nGraph.findVertices(query)
            return@autoclose result.map(UserIdentityVertexManipulator::toDao)
        }
    }

    private fun getFilter(permissions: Array<Permissions>): PagedResourceQueryBuilder.Filter? {
        if(permissions.isEmpty()) {
            return null
        }
        val paramName = "perm"
        val map = Collections.singletonMap(paramName, permissions.map(Permissions::shorthand).toTypedArray())
        val clause = projection(paramName).`in`(parameter(paramName))
        return PagedResourceQueryBuilder.Filter(clause, map)
    }
}
