package pl.eightandcounting.fragaria.repository.userRepository

import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.repository.PagedRepository
import pl.eightandcounting.fragaria.repository.Repository

interface UserRepository : Repository, PagedRepository<String, Permissions, UserIdentityVertex> {
    fun getUserByLogin(login: String): UserIdentityVertex?

    fun createUser(user: UserIdentityParams): UserIdentityVertex
    fun patchUser(user: UserIdentityParams, target: String): UserIdentityVertex?
    fun deleteUser(login: String): UserIdentityVertex?

    fun changePassword(login: String, oldPassword: String, newPassword: String): UserIdentityVertex?
}
