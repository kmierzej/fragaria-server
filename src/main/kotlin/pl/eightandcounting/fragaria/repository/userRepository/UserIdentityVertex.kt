package pl.eightandcounting.fragaria.repository.userRepository

import com.tinkerpop.blueprints.Element
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import org.mindrot.jbcrypt.BCrypt
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.UserIdentity
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.repository.Repository
import pl.eightandcounting.fragaria.repository.query.QueryWrapper

class UserIdentityGraph(val graph: OrientBaseGraph): Repository {
    fun findByLogin(login: String): UserIdentityVertexManipulator? {
        /*
        val vertices = graph.getVertices(
            UserIdentityVertexManipulator.CLASS,
            arrayOf(UserIdentityVertexManipulator.LOGIN),
            arrayOf(login)
        ) as Iterable<OrientVertex>;
        */
        val query = "SELECT expand(rid) FROM index:User.login where key =:key"
        val params = hashMapOf<String, Any>("key" to login)

        val wrapper = QueryWrapper(query, params)
        val result = graph.findVertices(wrapper)

        return result.map{UserIdentityVertexManipulator(it)}.firstOrNull()
    }

    fun findByLogin(login: Array<String>): List<UserIdentityVertexManipulator> {
        if(login.size == 1) {
            return listOf(findByLogin(login.first())).filterNotNull()
        }
        val query = "SELECT * FROM ${UserIdentityVertexManipulator.CLASS} WHERE login in :login"
        val params = HashMap<String, Any>(2)
        params.put("login", login)
        val wrapper = QueryWrapper(query, params)
        val result = graph.findVertices(wrapper)
        return result.map{UserIdentityVertexManipulator(it)}
    }

    fun deleteUser(user: UserIdentityVertexManipulator) {
        graph.removeVertex(user.vertex)
    }

    fun createUser(user: UserIdentityParams): UserIdentityVertexManipulator {
        val vertex = graph.addVertex("class:${UserIdentityVertexManipulator.CLASS}", user.params)
        return UserIdentityVertexManipulator(vertex)
    }

    fun findVertices(query: QueryWrapper): Iterable<UserIdentityVertexManipulator> {
        return graph.findVertices(query).map { UserIdentityVertexManipulator(it) }
    }
}

class UserIdentityVertexManipulator(val vertex: OrientVertex) {
    companion object {
        const val CLASS = "User"
        const val LOGIN = "login"
        const val EMAIL = "email"
        const val FULLNAME = "fullName"
        const val PASSWORD = "password"
        const val PERMISSIONS = "perm"
    }

    fun toDao(): UserIdentityVertex {
        val email = vertex.getStringProperty(EMAIL) ?: throw RuntimeException("YOLO")
        val fullname = vertex.getStringProperty(FULLNAME)
        val password = vertex.getStringProperty(PASSWORD) ?: throw RuntimeException("YOLO")

        val permissions = getPermissions()

        return UserIdentityVertex(
            login = getLogin(),
            email = email,
            fullname = fullname,
            password = password,
            permissions = permissions
        )
    }

    fun getLogin(): String {
        return vertex.getStringProperty(LOGIN) ?: throw RuntimeException("YOLO")
    }

    fun getPermissions(): Set<Permissions> {
        val permissionsFromDb = vertex.getProperty<Set<String>>(PERMISSIONS) ?: emptySet()
        return permissionsFromDb.map { Permissions.getPermission(it) }.filterNotNull().toSet()
    }

    fun setPassword(oldPassword: String, newPassword: String) {
        if (verifyPassword(oldPassword)) {
            vertex.setProperty(PASSWORD, newPassword)
        } else {
            throw ErrorMessageBasic.WRONG_PASSWORD.toException()
        }
    }

    fun verifyPassword(password: String): Boolean {
        val savedPassword =  vertex.getProperty<String>(PASSWORD)
        return BCrypt.checkpw(password, savedPassword)
    }

    fun setPermissions(permissions: Collection<Permissions>?) {
        if (permissions == null) {
            return
        }

        val newPerm = permissions.map(Permissions::shorthand)
        vertex.setProperty("perm", newPerm)
    }

    fun setProperties(identity: UserIdentityParams) {
        vertex.setProperties<OrientVertex>(identity.params)
    }

    private fun Element.getStringProperty(key: String): String? = this.getProperty<String>(key)
}

data class UserIdentityVertex(
    val login: String,
    val email: String,
    val fullname: String?,
    val password: String,
    val permissions: Set<Permissions>
) {
    fun toUserIdentity(): UserIdentity {
        return UserIdentity(
            login = this.login,
            email = this.email,
            fullname = this.fullname,
            permissions = this.permissions
        )
    }
}

class UserIdentityParams(
    val login: String,
    val password: String?,
    email: String?,
    fullname: String?,
    permissions: Set<Permissions>?
) {
    val params = HashMap<String, Any>()
    init {
        fun HashMap<String, Any>.putConditional(string: String, value: Any?) { if(value != null) { this.put(string, value) }}
        params.put(UserIdentityVertexManipulator.LOGIN, login)
        params.putConditional(UserIdentityVertexManipulator.EMAIL, email)
        params.putConditional(UserIdentityVertexManipulator.PASSWORD, password)
        params.putConditional(UserIdentityVertexManipulator.FULLNAME, fullname)
        if(permissions != null) {
            params.put(UserIdentityVertexManipulator.PERMISSIONS, permissions.map(Permissions::shorthand))
        }
    }
}
