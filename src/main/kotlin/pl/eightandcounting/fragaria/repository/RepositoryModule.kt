package pl.eightandcounting.fragaria.repository

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import io.dropwizard.jackson.Jackson
import pl.eightandcounting.fragaria.repository.projectAssigneesRepository.ProjectAssigneesRepository
import pl.eightandcounting.fragaria.repository.projectAssigneesRepository.ProjectAssigneesRepositoryImpl
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectRepository
import pl.eightandcounting.fragaria.repository.projectRepository.ProjectRepositoryImpl
import pl.eightandcounting.fragaria.repository.taskRepository.TaskRepository
import pl.eightandcounting.fragaria.repository.taskRepository.TaskRepositoryImpl
import pl.eightandcounting.fragaria.repository.timesheetRepository.TimesheetRepository
import pl.eightandcounting.fragaria.repository.timesheetRepository.TimesheetRepositoryImpl
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenRepository
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenRepositoryImpl
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenVerifyRepository
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenVerifyRepositoryImpl
import pl.eightandcounting.fragaria.repository.userRepository.UserRepository
import pl.eightandcounting.fragaria.repository.userRepository.UserRepositoryImpl

val MAPPER = Jackson.newObjectMapper()
val repositoryModule = Kodein.Module {
    bind<UserRepository>() with provider { UserRepositoryImpl(instance()) }
    bind<ProjectRepository>() with provider { ProjectRepositoryImpl(instance(), MAPPER) }
    bind<ProjectAssigneesRepository>() with provider { ProjectAssigneesRepositoryImpl(instance()) }
    bind<TaskRepository>() with provider { TaskRepositoryImpl(instance()) }
    bind<TimesheetRepository>() with provider { TimesheetRepositoryImpl(instance())}
    bind<TokenRepository>() with provider { TokenRepositoryImpl(instance(), instance("tokenStrategy")) }
    bind<TokenVerifyRepository>() with provider { TokenVerifyRepositoryImpl(instance(), instance("tokenStrategy")) }
}
