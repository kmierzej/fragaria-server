package pl.eightandcounting.fragaria.repository

enum class DatasetType {
    INITIAL,
    DEMO
}
