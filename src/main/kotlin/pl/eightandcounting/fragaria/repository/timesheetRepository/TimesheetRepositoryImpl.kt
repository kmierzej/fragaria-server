package pl.eightandcounting.fragaria.repository.timesheetRepository

import com.orientechnologies.orient.core.storage.ORecordDuplicatedException
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.impls.orient.OrientEdge
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.api.TimeEntry
import pl.eightandcounting.fragaria.error.getUnknownTaskException
import pl.eightandcounting.fragaria.error.getUnknownUserException
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.getParamsMap
import pl.eightandcounting.fragaria.repository.query.QueryWrapper
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityGraph
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityVertexManipulator
import java.time.LocalDate
import java.util.*

class TimesheetRepositoryImpl(private val database: OrientGraphFactory): TimesheetRepository {
    override fun getByTask(taskId: CompositeTaskId, from: LocalDate, to: LocalDate): List<TimeEntry> {
        return autoclose(database.noTx) { graph ->
            val task = graph.getVertices(
                Task.CLASS,
                arrayOf(Task.PROJECT_ID, Task.SEQ),
                arrayOf(taskId.projectId, taskId.id)
            ).firstOrNull() ?: throw getUnknownTaskException()

            val params = getParamsMap()
            params.put("to", java.sql.Date.valueOf(to))
            params.put("frm", java.sql.Date.valueOf(from))
            params.put("task", task)
            val query = "SELECT FROM ${TimeEntry.CLASS} WHERE dt >= :frm AND dt < :to AND out=:task"

            val wrapper = QueryWrapper(query, params)

            val result  = graph.findEdges(wrapper)
            return@autoclose result.map { extractTimeEntry(it) }
        }
    }

    override fun getByProject(projectId: String, from: LocalDate, to: LocalDate): List<TimeEntry> {
        return autoclose(database.noTx) { graph ->
            val tasks = graph.getVertices(Task.CLASS, arrayOf(Task.PROJECT_ID), arrayOf(projectId))

            if(tasks.none()) { return emptyList() }

            val params = getParamsMap()
            params.put("to", java.sql.Date.valueOf(to))
            params.put("frm", java.sql.Date.valueOf(from))
            params.put("task", tasks)
            val query = "SELECT FROM ${TimeEntry.CLASS} WHERE dt >= :frm AND dt < :to AND out IN :task"

            val wrapper = QueryWrapper(query, params)

            val result  = graph.findEdges(wrapper)
            return@autoclose result.map { extractTimeEntry(it) }
        }
    }

    override fun getByUser(userId: String, from: LocalDate, to: LocalDate): List<TimeEntry> {
        return autoclose(database.noTx) { graph ->
            val uGraph = UserIdentityGraph(graph)
            val user = uGraph.findByLogin(userId) ?: throw getUnknownUserException()

            val params = getParamsMap()
            params.put("to", java.sql.Date.valueOf(to))
            params.put("frm", java.sql.Date.valueOf(from))
            params.put("user", user.vertex)
            val query = "SELECT FROM ${TimeEntry.CLASS} WHERE dt >= :frm AND dt < :to AND in=:user"

            val wrapper = QueryWrapper(query, params)
            val result  = graph.findEdges(wrapper)
            return@autoclose result.map { extractTimeEntry(it) }
        }
    }

    override fun get(from: LocalDate, to: LocalDate): List<TimeEntry> {
        return autoclose(database.noTx) { graph ->
            val params = getParamsMap()
            params.put("to", java.sql.Date.valueOf(to))
            params.put("frm", java.sql.Date.valueOf(from))
            val query = "SELECT FROM ${TimeEntry.CLASS} WHERE dt >= :frm AND dt < :to"

            val wrapper = QueryWrapper(query, params)

            val result  = graph.findEdges(wrapper)
            return@autoclose result.map { extractTimeEntry(it) }
        }
    }

    private fun extractTimeEntry(edge: OrientEdge): TimeEntry {
        val user = UserIdentityVertexManipulator(edge.getVertex(Direction.IN)).getLogin()
        val projectId = edge.getVertex(Direction.OUT).getProperty<String>(Task.PROJECT_ID)
        val seq = edge.getVertex(Direction.OUT).getProperty<Short>(Task.SEQ)
        val task = CompositeTaskId(seq, projectId).toString()
        val date = edge.getProperty<Date>("dt")
        val time = edge.getProperty<Short>("time")

        @Suppress("DEPRECATION")
        val localDate = LocalDate.of(date.year + 1900, date.month + 1, date.date)
        return TimeEntry(user = user, task = task, date = localDate , time = time)
    }

    override fun create(taskId: CompositeTaskId, userId: String, date: LocalDate, time: Short) {
        return autoclose(database.tx) { graph ->
            val uGraph = UserIdentityGraph(graph)
            val user = uGraph.findByLogin(userId) ?: throw getUnknownUserException()

            val task = graph.getVertices(
                Task.CLASS,
                arrayOf(Task.PROJECT_ID, Task.SEQ),
                arrayOf(taskId.projectId, taskId.id)
            ).firstOrNull() ?: throw getUnknownTaskException()

            val project = task.getVertices(Direction.IN, "hasTask").firstOrNull() ?: throw getUnknownTaskException()

            val TOTAL_LOGGED = "totalLogged"
            val beforeTask: Int = task.getProperty<Int>(TOTAL_LOGGED)
            val beforeProject: Int = project.getProperty<Int>(TOTAL_LOGGED)
            task.setProperty(TOTAL_LOGGED, beforeTask + time)
            project.setProperty(TOTAL_LOGGED, beforeProject + time)

            val params = getParamsMap()
            params.put("user", user.vertex)
            params.put("task", task)
            params.put("date", java.sql.Date.valueOf(date))
            params.put("time", time)
            val query = "CREATE EDGE ${TimeEntry.CLASS} FROM :task TO :user SET dt=:date, time=:time"

            val wrapper = QueryWrapper(query, params)

            try {
                graph.execute(wrapper)
                graph.commit()
            } catch (e: ORecordDuplicatedException) {
                val vertex: Edge? = graph.getEdge(e.rid)
                if (vertex != null) {
                    vertex.setProperty(TimeEntry.TIME, time)
                    return
                }
                throw getUnknownTaskException()
            }
        }
    }

    override fun delete(taskId: CompositeTaskId, userId: String, date: LocalDate) {
        autoclose(database.tx) { graph ->
            val uGraph = UserIdentityGraph(graph)
            val user = uGraph.findByLogin(userId) ?: throw getUnknownUserException()

            val task = graph.getVertices(
                Task.CLASS,
                arrayOf(Task.PROJECT_ID, Task.SEQ),
                arrayOf(taskId.projectId, taskId.id)
            ).firstOrNull() ?: throw getUnknownTaskException()

            val project = task.getVertices(Direction.IN, "hasTask").firstOrNull() ?: throw getUnknownTaskException()

            val params = getParamsMap()
            params.put("user", user.vertex)
            params.put("task", task)
            //params.put("date", java.sql.Date.valueOf(date))

            val query = "SELECT * from ${TimeEntry.CLASS} WHERE out= :task AND in= :user AND dt='$date'"
            val wrapper = QueryWrapper(query, params)

            val edge = graph.findEdges(wrapper).firstOrNull() ?: return@autoclose

            val TOTAL_LOGGED = "totalLogged"
            val beforeTask: Int = task.getProperty<Int>(TOTAL_LOGGED)
            val beforeProject: Int = project.getProperty<Int>(TOTAL_LOGGED)
            val time = edge.getProperty<Short>("time")
            task.setProperty(TOTAL_LOGGED, beforeTask - time)
            project.setProperty(TOTAL_LOGGED, beforeProject - time)

            edge.remove()
        }
    }
}
