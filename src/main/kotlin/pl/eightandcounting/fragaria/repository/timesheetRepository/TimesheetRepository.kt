package pl.eightandcounting.fragaria.repository.timesheetRepository

import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.TimeEntry
import pl.eightandcounting.fragaria.repository.Repository
import java.time.LocalDate


interface TimesheetRepository: Repository {
    fun getByTask(taskId: CompositeTaskId, from: LocalDate, to: LocalDate): List<TimeEntry>
    fun getByProject(projectId: String, from: LocalDate, to: LocalDate): List<TimeEntry>
    fun getByUser(userId: String, from: LocalDate, to: LocalDate): List<TimeEntry>
    fun get(from: LocalDate, to: LocalDate): List<TimeEntry>

    fun create(taskId: CompositeTaskId, userId: String, date: LocalDate, time: Short)
    fun delete(taskId: CompositeTaskId, userId: String, date: LocalDate)
}
