package pl.eightandcounting.fragaria.repository.taskRepository

import com.tinkerpop.blueprints.Vertex
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.repository.VertexWrapper
import pl.eightandcounting.fragaria.repository.get
import pl.eightandcounting.fragaria.repository.getEnum
import pl.eightandcounting.fragaria.repository.getOptional
import java.util.*

class TaskVertex(val vertex: Vertex): VertexWrapper<Task> {
    override fun getCanonicalIdentity(): Task {
        return getIdentity()
    }

    override fun getProjectedIdentity(): Task {
        return getIdentity()
    }

    private fun getIdentity(): Task {
        val id = vertex.get<String>(Task.PROJECT_ID) + "-" + vertex.get<Short>(Task.SEQ)
        return Task(
            id = id,
            title = vertex.get(Task.TITLE),
            status = vertex.getEnum(Task.STATUS, Task.Status.CLOSED),
            description = vertex.getOptional(Task.DESCRIPTION),

            estimatedTime = vertex.get("estimatedTime"),
            totalLogged = vertex.get("totalLogged"),

            created = vertex.get(Task.CREATED),
            updated = vertex.get(Task.UPDATED)
        )
    }

    override fun patchParameters(identity: Task) {
        identity.getParams().forEach { key, value -> vertex.setProperty(key, value) }
        vertex.setProperty(Task.UPDATED, Date())
    }
}
