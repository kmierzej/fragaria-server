package pl.eightandcounting.fragaria.repository.taskRepository

import com.orientechnologies.orient.core.command.script.OCommandScript
import com.orientechnologies.orient.core.exception.OValidationException
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.error.ErrorMessageBasic
import pl.eightandcounting.fragaria.error.getMissingParamException
import pl.eightandcounting.fragaria.error.getUnknownProjectException
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder.Filter.Companion.addFilters
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder.Filter.Companion.simpleEqualsClause
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder.Filter.Companion.simpleInClause
import java.util.*

class TaskRepositoryImpl (private val database: OrientGraphFactory) : TaskRepository {
    val queryBuilder = PagedResourceQueryBuilder(
        key = Task.SEQ,
        clazz = Task.CLASS,
        projections = arrayOf("*")
    )

    override fun deleteTask(id: CompositeTaskId): Task? {
        return autoclose(database.tx) { graph ->
            // TODO: DUPLICATED WHERE on id CHECK
            val query = queryBuilder.getDeleteQuery(id, getProjectFilter(id))
            val result = graph.findVertices(query).firstOrNull() ?: return null

            return@autoclose TaskVertex(result).getProjectedIdentity()
        }
    }

    override fun createTask(task: Task, projectId: String): Task {
        if(task.title == null || task.title.isBlank()) {
            throw getMissingParamException(Task.TITLE)
        }
        val graph = database.noTx
        val now = Date()

        val params = task.getParams()
        params.put(Task.PROJECT_ID, projectId)
        params.put(Task.CREATED, now)
        params.put(Task.UPDATED, now)
        params.putIfAbsent(Task.STATUS, Task.Status.OPEN)

        val query = "BEGIN \n" +
            "let \$project = SELECT @rid FROM Project where id=:projectId\n"+
            "let \$counter = update Project INCREMENT seq=1 RETURN AFTER \$current.seq where @rid=\$project[0].rid\n"+
            "let \$e = " + createVertex(Task.CLASS, params) + ", seq=\$counter.value[0]\n" +
            "CREATE EDGE hasTask FROM \$project[0].rid TO \$e\n" +
            "COMMIT \n" +
            "return \$e"

        try {
            val vertex = graph.command(OCommandScript(query)).execute<Vertex>(params)
            return TaskVertex(vertex).getCanonicalIdentity()
        } catch (e: ORecordDuplicatedException) {
            throw ErrorMessageBasic.DUPLICATED_TITLE.toException()
        } catch (e: OValidationException) {
            throw getUnknownProjectException()
        } finally {
            graph.shutdown()
        }
    }

    private fun createVertex(clazz: String, params: HashMap<String, Any>): String {
        val prefix = "CREATE VERTEX $clazz SET "
        val query = params.keys.map { "$it=:$it" }.joinToString(prefix = prefix, separator = ",")
        return query
    }

    override fun patchTask(task: Task, id: CompositeTaskId): Task? {
        return autoclose(database.tx) { graph ->
            val query = queryBuilder.getQuery(getProjectFilter(id))
            val wrapper = graph.findVertices(query).map(::TaskVertex).firstOrNull() ?: return null

            wrapper.patchParameters(task)
            return@autoclose wrapper.getCanonicalIdentity()
        }
    }

    override fun getTaskById(id: CompositeTaskId): Task? {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getQuery(getProjectFilter(id))
            val result = graph.findVertices(query).map(::TaskVertex).firstOrNull() ?: return@autoclose null
            return result.getProjectedIdentity()
        }
    }

    override fun getBeforeId(id: CompositeTaskId, limit: Int, status: Array<Task.Status>): Collection<Task> {
        return autoclose(database.noTx) { graph ->
            val filters =  getProjectAndStatusFilter(status, id.projectId)
            val beforeQuery = queryBuilder.getBeforeQuery(id.id.toString(), limit, filters)
            val result = graph.findVertices(beforeQuery).map(::TaskVertex).reversed().toMutableList()
            if(result.size < limit) {
                val afterQuery = queryBuilder.getAfterQuery(id.id.toString(), limit - result.size, filters, inclusive = true)
                val result2 = graph.findVertices(afterQuery).map(::TaskVertex)
                result.addAll(result2)
            }
            return@autoclose result.map { it.getProjectedIdentity()}
        }
    }

    override fun getFromId(id: CompositeTaskId, limit: Int, status: Array<Task.Status>): Collection<Task> {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getFromQuery(id.id.toString(), limit, getProjectAndStatusFilter(status, id.projectId))
            val result = graph.findVertices(query).map(::TaskVertex).map { it.getProjectedIdentity()}
            return@autoclose result
        }
    }

    override fun getAfterId(id: CompositeTaskId, limit: Int, status: Array<Task.Status>): Collection<Task> {
        return autoclose(database.noTx) { graph ->
            val query = queryBuilder.getAfterQuery(id.id.toString(), limit, getProjectAndStatusFilter(status, id.projectId))
            return@autoclose graph.findVertices(query).map(::TaskVertex).map { it.getProjectedIdentity()}
        }
    }

    private fun getProjectAndStatusFilter(status: Array<Task.Status>, projectId: String): PagedResourceQueryBuilder.Filter {
        val projectFilter = simpleEqualsClause(Task.PROJECT_ID, projectId)
        if(status.isEmpty()) {
            return projectFilter;
        }

        val statusFilter = simpleInClause(Task.STATUS, status.map(Task.Status::toString).toTypedArray())
        return addFilters(projectFilter, statusFilter)
    }

    private fun getProjectFilter(projectId: CompositeTaskId): PagedResourceQueryBuilder.Filter {
        val projectFilter = simpleEqualsClause(Task.PROJECT_ID, projectId.projectId)
        val seqFilter = simpleEqualsClause(Task.SEQ, projectId.id)
        return addFilters(projectFilter, seqFilter)
    }
}
