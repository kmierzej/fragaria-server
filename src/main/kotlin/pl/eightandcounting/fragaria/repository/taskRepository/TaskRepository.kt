package pl.eightandcounting.fragaria.repository.taskRepository

import pl.eightandcounting.fragaria.api.CompositeTaskId
import pl.eightandcounting.fragaria.api.Task
import pl.eightandcounting.fragaria.repository.PagedRepository
import pl.eightandcounting.fragaria.repository.Repository

interface TaskRepository : Repository, PagedRepository<CompositeTaskId, Task.Status, Task> {
    fun getTaskById(id: CompositeTaskId): Task?

    fun createTask(task: Task, projectId: String): Task?
    fun patchTask(task: Task, id: CompositeTaskId): Task?
    fun deleteTask(id: CompositeTaskId): Task?
}
