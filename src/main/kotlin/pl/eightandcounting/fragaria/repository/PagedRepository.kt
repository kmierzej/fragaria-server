package pl.eightandcounting.fragaria.repository

import java.util.*

interface PagedRepository<T, R, S> {
    fun getFromId(id: T, limit: Int, status: Array<R>): Collection<S>
    fun getBeforeId(id: T, limit: Int, status: Array<R>): Collection<S>
    fun getAfterId(id: T, limit: Int, status: Array<R>): Collection<S>
}

enum class PagedKeys {
    BEFORE {
        override fun <T, R, S> callMethod(repo: PagedRepository<T, R, S>, id: T, limit: Int, status: Array<R>): Collection<S> {
            return repo.getBeforeId(id, limit, status)
        }
    },
    AFTER {
        override fun <T, R, S> callMethod(repo: PagedRepository<T, R, S>, id: T, limit: Int, status: Array<R>): Collection<S> {
            return repo.getAfterId(id, limit, status)
        }
    },
    FROM {
        override fun <T, R, S> callMethod(repo: PagedRepository<T, R, S>, id: T, limit: Int, status: Array<R>): Collection<S> {
            return repo.getFromId(id, limit, status)
        }
    };

    abstract fun <T, R, S>callMethod(repo: PagedRepository<T, R, S>, id: T, limit: Int, status: Array<R>): Collection<S>
}

fun <E>withLabel(label: String, toWrap: E): Map<String, E> {
    return Collections.singletonMap(label, toWrap)
}

inline fun <reified T>transformStatus(received: List<T>, default: Array<T>): Array<T> {
    if(received.isEmpty()) {
        return default
    }
    return received.toTypedArray()
}

fun <T>chooseParams(before: Optional<T>, after: Optional<T>, from: Optional<T>, default: T): Pair<PagedKeys, T> {
    if(before.isPresent) return Pair(PagedKeys.BEFORE, before.get())
    if(after.isPresent) return Pair(PagedKeys.AFTER, after.get())
    if(from.isPresent) return Pair(PagedKeys.FROM, from.get())
    return Pair(PagedKeys.FROM, default)
}

fun verifyPagingLimit(limit: Int, max: Int = 100, default: Int = 10): Int {
    if( limit <= 0 ) return default
    else if (limit >= max) return max
    else return limit
}
