package pl.eightandcounting.fragaria.repository

import com.fasterxml.jackson.databind.ObjectMapper
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph
import com.tinkerpop.blueprints.impls.orient.OrientDynaElementIterable
import com.tinkerpop.blueprints.impls.orient.OrientEdge
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import pl.eightandcounting.fragaria.repository.query.QueryWrapper

interface Repository {
    fun OrientBaseGraph.findVertices(query: QueryWrapper): Iterable<OrientVertex> {
        val vertices: OrientDynaElementIterable = this.find(query)
        return vertices.asIterable() as Iterable<OrientVertex>
    }

    fun OrientBaseGraph.findEdges(query: QueryWrapper): Iterable<OrientEdge> {
        val vertices: OrientDynaElementIterable = this.find(query)
        return vertices.asIterable() as Iterable<OrientEdge>
    }

    fun <T>OrientBaseGraph.find(query: QueryWrapper): T = this.command(query.getCommand()).execute<T>(query.params)
    fun OrientBaseGraph.execute(query: QueryWrapper): Any? = this.command(query.getCommand()).execute<Any>(query.params)
    fun <T>ObjectMapper.getParamsMap(item: T) = this.convertValue(item, Map::class.java) as MutableMap<String, Any>
}

fun getParamsMap(): HashMap<String,Any> = HashMap()

inline fun <T> autoclose(graph: OrientBaseGraph, body: (OrientBaseGraph) -> T): T {
    try {
        return body(graph)
    }
    finally {
        graph.shutdown()
    }
}

