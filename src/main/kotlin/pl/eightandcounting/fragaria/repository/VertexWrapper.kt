package pl.eightandcounting.fragaria.repository

import com.tinkerpop.blueprints.Element

interface VertexWrapper<T> {
    fun getCanonicalIdentity(): T
    fun getProjectedIdentity(): T
    fun patchParameters(identity: T)
}

fun <T> Element.get(key: String): T = this.getProperty(key) ?: throw RuntimeException("Missing parameter $key")
fun Element.getString(key: String): String = this.getProperty(key) ?: throw RuntimeException("Missing parameter $key")
fun <T> Element.getOptional(key: String): T? = this.getProperty(key)

inline fun <reified T : kotlin.Enum<T>> Element.getEnum(key: String, orElse: T): T {
    val type: String = this.getOptional(key) ?: return orElse
    return java.lang.Enum.valueOf(T::class.java, type)
}

inline fun <reified T : kotlin.Enum<T>> Element.getOptionalEnum(key: String): T? {
    val type: String = this.getOptional(key) ?: return null
    return java.lang.Enum.valueOf(T::class.java, type)
}
