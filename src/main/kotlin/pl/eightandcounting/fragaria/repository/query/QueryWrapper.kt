package pl.eightandcounting.fragaria.repository.query

import com.orientechnologies.orient.core.sql.OCommandSQL

data class QueryWrapper(val query: String, val params: Map<String, Any>) {
    fun getCommand() = OCommandSQL(query)
}
