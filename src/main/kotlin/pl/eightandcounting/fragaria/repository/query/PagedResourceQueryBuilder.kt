package pl.eightandcounting.fragaria.repository.query

import com.github.raymanrt.orientqb.query.Clause
import com.github.raymanrt.orientqb.query.Parameter.parameter
import com.github.raymanrt.orientqb.query.Projection.projection
import com.github.raymanrt.orientqb.query.Query
import com.gitub.raymanrt.orientqb.delete.Delete
import com.gitub.raymanrt.orientqb.delete.ReturnStrategy
import java.util.*
import kotlin.collections.HashMap

class PagedResourceQueryBuilder(
    private val key: String,
    private var clazz: String,
    private val projections: Array<String>
) {
    private val KEY = projection(key)
    private val KEY_PARAM = parameter("key")
    private val LIMIT_PARAM = parameter("limit").toString()

    private val GET_QUERY: String
    init { this.GET_QUERY = prepareBaseQuery(null).where(KEY.eq(KEY_PARAM)).toString() }

    fun getQuery(key: String): QueryWrapper {
       return QueryWrapper(this.GET_QUERY, hashMapOf(Pair("key", key)))
    }

    fun getQuery(filters: Filter): QueryWrapper {
        val query = prepareBaseQuery(filters)

        val args = if (filters.arguments is HashMap<String, Any>) {
            filters.arguments
        }
        else {
            val map = HashMap<String, Any>()
            map.putAll(filters.arguments)
            map
        }
        return QueryWrapper(query.toString(), args)
    }

    fun getDeleteQuery(key: Any, filters: Filter?): QueryWrapper {
        val query = Delete().from( "VERTEX " + clazz).where(KEY.eq(key)).returnStrategy(ReturnStrategy.BEFORE)

        val map: HashMap<String, Any> = HashMap()
        map.put("key", key)

        if(filters != null) {
            query.where(filters.filter)
            map.putAll(filters.arguments)
        }

        return QueryWrapper(query.toString(), map)
    }

    fun getBeforeQuery(key: String, limit: Int, filters: Filter?): QueryWrapper {
        val query = prepareBaseQuery(filters).where(KEY.lt(KEY_PARAM)).orderByDesc(this.key).limit(LIMIT_PARAM)
        return QueryWrapper(query.toString(), getParameters(key, filters, limit))
    }

    fun getAfterQuery(key: String, limit: Int, filters: Filter?, inclusive: Boolean = false): QueryWrapper {
        val compareParam = if(inclusive) { KEY.ge(KEY_PARAM) } else { KEY.gt(KEY_PARAM) }
        val query = prepareBaseQuery(filters).where(compareParam).orderBy(this.key).limit(LIMIT_PARAM)
        return QueryWrapper(query.toString(), getParameters(key, filters, limit))
    }

    fun getFromQuery(key: String, limit: Int, filters: Filter?): QueryWrapper {
        val query = prepareBaseQuery(filters).where(KEY.ge(KEY_PARAM)).orderBy(this.key).limit(LIMIT_PARAM)
        return QueryWrapper(query.toString(), getParameters(key, filters, limit))
    }

    private fun prepareBaseQuery(filters: Filter?): Query {
        var base = Query().select(*projections).from(clazz)

        if(filters != null && filters.arguments.isNotEmpty()) {
            base = base.where(filters.filter)
        }

        if(projections.contains("*") || projections.contains(key)) {
            return base
        }

        return base.select(key)
    }

    private fun getParameters(key: Any, filters: Filter?, limit: Int): HashMap<String, Any> {
        val map: HashMap<String, Any> = HashMap()
        map.put("key", key)
        map.put("limit", limit)
        if(filters != null) {
            map.putAll(filters.arguments)
        }
        return map
    }

    data class Filter(val filter: Clause, val arguments: Map<String, Any>){
        companion object {
            fun simpleEqualsClause(projection: String, value: Any): Filter {
                val clause = projection(projection).eq(parameter(projection))
                return Filter(clause, Collections.singletonMap(projection, value))
            }

            fun simpleInClause(projection: String, value: Array<String>): Filter {
                val clause = projection(projection).`in`(parameter(projection))
                return Filter(clause, Collections.singletonMap(projection, value))
            }

            fun addFilters(a: Filter, b: Filter?): Filter {
                if(b == null) {
                    return a
                }
                val clause = Clause.and(a.filter, b.filter)
                val map = HashMap<String, Any>()
                map.putAll(a.arguments)
                map.putAll(b.arguments)

                return Filter(clause, map)
            }
        }
    }
}
