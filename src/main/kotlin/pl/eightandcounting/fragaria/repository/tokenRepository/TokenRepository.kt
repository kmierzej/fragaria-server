package pl.eightandcounting.fragaria.repository.tokenRepository

import pl.eightandcounting.fragaria.repository.Repository

interface TokenRepository: Repository {
    fun saveToken(login: String, token: TokenDB): Boolean
    fun invalidateAndSaveToken(login: String, token: TokenDB)

    fun getTokens(login: String): Iterable<TokenDB>?
    fun invalidateToken(login: String, tokenHash: String)
    fun cleanTokens()
}
