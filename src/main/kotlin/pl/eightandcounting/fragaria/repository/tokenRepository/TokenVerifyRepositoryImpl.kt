package pl.eightandcounting.fragaria.repository.tokenRepository

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import com.tinkerpop.blueprints.impls.orient.OrientVertex
import pl.eightandcounting.fragaria.model.auth.TokenCredentials
import pl.eightandcounting.fragaria.model.auth.VerifiedTokenCredentials
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityGraph
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityVertexManipulator

class TokenVerifyRepositoryImpl(
    private val database: OrientGraphFactory,
    private val verifyStrategy: VerifyStrategy = VerifyStrategy.WHITELISTING
): TokenVerifyRepository {

    override fun verifyToken(login: TokenCredentials, token: TokenDB): VerifiedTokenCredentials? {
        return autoclose(database.tx) { graph ->
            val vertex = graph.findToken(token.tokenHash)
            if(vertex == null) {
                if(verifyStrategy == VerifyStrategy.WHITELISTING) {
                    return@autoclose null
                } else if (verifyStrategy == VerifyStrategy.BLACKLISTING) {
                    val uGraph = UserIdentityGraph(graph)
                    val user = uGraph.findByLogin(login.login) ?: return@autoclose null
                    val vertex = graph.createToken(token)
                    graph.addEdge("class:hasSession", vertex, user.vertex, "hasSession")

                    val allPermissions = user.getPermissions()
                    val permissions = user.getPermissions().toMutableSet()
                    permissions.retainAll(login.permissions)

                    val credentials = VerifiedTokenCredentials(
                        login = login.login,
                        expirationTime = login.expirationTime,
                        permissions = permissions,
                        allPermissions = allPermissions)
                    return credentials;
                } else {
                    TODO("NOT IMPLEMENTED")
                }
            }

            if(!vertex.getProperty<Boolean>(TOKEN_VALID)) {
                return null
            }

            val user = vertex.getVertices(Direction.OUT, "hasSession").map { UserIdentityVertexManipulator(it as OrientVertex) }.first()
            val allPermissions = user.getPermissions()
            val permissions = user.getPermissions().toMutableSet()
            permissions.retainAll(login.permissions)
            val credentials = VerifiedTokenCredentials(
                login = user.getLogin(),
                expirationTime = login.expirationTime,
                permissions = permissions,
                allPermissions = allPermissions)
            return@autoclose credentials
        }
    }
}
