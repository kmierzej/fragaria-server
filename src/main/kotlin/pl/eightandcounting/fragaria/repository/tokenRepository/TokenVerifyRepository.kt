package pl.eightandcounting.fragaria.repository.tokenRepository

import pl.eightandcounting.fragaria.model.auth.TokenCredentials
import pl.eightandcounting.fragaria.model.auth.VerifiedTokenCredentials
import pl.eightandcounting.fragaria.repository.Repository

interface TokenVerifyRepository: Repository {
    fun verifyToken(login: TokenCredentials, token: TokenDB): VerifiedTokenCredentials?
}
