package pl.eightandcounting.fragaria.repository.tokenRepository

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory
import pl.eightandcounting.fragaria.repository.autoclose
import pl.eightandcounting.fragaria.repository.getParamsMap
import pl.eightandcounting.fragaria.repository.query.QueryWrapper
import pl.eightandcounting.fragaria.repository.userRepository.UserIdentityGraph
import java.util.*

class TokenRepositoryImpl(
    private val database: OrientGraphFactory,
    private val verifyStrategy: VerifyStrategy = VerifyStrategy.WHITELISTING
): TokenRepository {
    override fun getTokens(login: String): Iterable<TokenDB>? {
        return autoclose(database.tx) { graph ->
            val uGraph = UserIdentityGraph(graph)
            val user = uGraph.findByLogin(login)?: return@autoclose null

            val tokens = user.vertex.getVertices(Direction.IN, "hasSession")
            val dbTokens = tokens.map { fromVertex(it) }.filter { it.valid }

            return@autoclose dbTokens
        }
    }

    override fun saveToken(login: String, token: TokenDB): Boolean {
        return autoclose(database.tx) { graph ->
            val uGraph = UserIdentityGraph(graph)
            val user = uGraph.findByLogin(login)?: return@autoclose false

            val token = graph.addVertex("class:$TOKEN_CLASS", token.getParams())
            graph.addEdge("class:hasSession", token, user.vertex, "hasSession")
            return@autoclose true
        }
    }

    override fun cleanTokens() {
        val now = System.currentTimeMillis()
        val query = "DELETE VERTEX Token WHERE ExpTime < :now"
        val params = Collections.singletonMap("now", now)

        val wrapper = QueryWrapper(query, params)

        autoclose(database.noTx) { graph ->
            graph.execute(wrapper)
        }
    }

    override fun invalidateToken(login: String, tokenHash: String) {
        val query = if(verifyStrategy == VerifyStrategy.WHITELISTING) {
            "DELETE VERTEX Token WHERE TokenHash=:tokenHash AND out(hasSession).login = :login"
        } else {
            "UPDATE Token SET valid = false WHERE TokenHash=:tokenHash AND out(hasSession).login = :login"
        }

        val params = getParamsMap()
        params.put("tokenHash", tokenHash)
        params.put("login", login)

        val wrapper = QueryWrapper(query, params)

        autoclose(database.tx) { graph ->
            graph.execute(wrapper)
        }
    }

    override fun invalidateAndSaveToken(login: String, token: TokenDB) {
        return autoclose(database.tx) { graph ->
            val oldVertex = graph.findToken(token.tokenHash)

            if(oldVertex != null) {
                if(verifyStrategy == VerifyStrategy.WHITELISTING) {
                    oldVertex.remove()
                } else {
                    oldVertex.setProperty(TOKEN_VALID, false)
                }
            } else if (verifyStrategy == VerifyStrategy.BLACKLISTING) {
                val uGraph = UserIdentityGraph(graph)
                val token = graph.addVertex("class:$TOKEN_CLASS", token.getParams())
                token.setProperty(TOKEN_VALID, false)
                val user = uGraph.findByLogin(login)

                if( user != null ) {
                    graph.addEdge("class:hasSession", token, user.vertex, "hasSession")
                }
            }
        }
    }
}
