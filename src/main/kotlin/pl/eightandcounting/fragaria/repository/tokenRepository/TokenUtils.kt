package pl.eightandcounting.fragaria.repository.tokenRepository

import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph

const val TOKEN_CLASS = "Token"
const val TOKEN_VALID = "valid"

enum class VerifyStrategy { BLACKLISTING, WHITELISTING }

fun OrientBaseGraph.findToken(tokenHash: String): Vertex? {
    return this.getVertices(
        TOKEN_CLASS,
        arrayOf("TokenHash"),
        arrayOf(tokenHash)
    ).firstOrNull()
}

fun OrientBaseGraph.createToken(token: TokenDB): Vertex {
    return this.addVertex("class:$TOKEN_CLASS", token.getParams())
}
