package pl.eightandcounting.fragaria.repository.tokenRepository

import com.tinkerpop.blueprints.Vertex

data class TokenDB (
    val userAgent: String,
    val tokenHash: String,
    val valid: Boolean,
    val lastAccess: Long,
    val expTime: Long,
    val source: String
) {
    fun getParams(): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        map.put("UserAgent", userAgent)
        map.put("TokenHash", tokenHash)
        map.put("valid", valid)
        map.put("LastAccess", lastAccess)
        map.put("ExpTime", expTime)
        map.put("Source", source)
        return map
    }

}

fun fromVertex(vertex: Vertex): TokenDB {
    val userAgent = vertex.getProperty<String>("UserAgent")
    val tokenHash = vertex.getProperty<String>("TokenHash")
    val valid = vertex.getProperty<Boolean>("valid")
    val lastAccess = vertex.getProperty<Long>("LastAccess")
    val expTime = vertex.getProperty<Long>("ExpTime")
    val source = vertex.getProperty<String>("Source")

    return TokenDB(
        userAgent = userAgent,
        tokenHash = tokenHash,
        valid = valid,
        lastAccess = lastAccess,
        expTime = expTime,
        source = source)
}
