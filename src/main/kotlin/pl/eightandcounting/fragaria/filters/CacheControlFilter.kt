package pl.eightandcounting.fragaria.filters

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CacheControlFilter: Filter {
    val endings = arrayOf("js", "css", "js.gz", "css.gz", "eot", "woff", "woff2", "svg", "ttf")

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        response as HttpServletResponse
        request as HttpServletRequest

        val pathInfo = request.pathInfo
        if( pathInfo.hasEnding(this.endings)) {
            response.setHeader("Cache-Control", "public, max-age=${year}")
        }
        chain.doFilter(request, response)
    }

    private fun String.hasEnding(endings: Array<String>): Boolean {
        return endings.any { this.endsWith(it) }
    }

    override fun destroy() { }
    override fun init(config: FilterConfig) { }

    companion object {
        const val year = 31556926
    }
}

