package pl.eightandcounting.fragaria.model.auth

import pl.eightandcounting.fragaria.api.Permissions

data class TokenPair(
    val accessTokens: List<Token>,
    val refreshToken: Token
)

data class Token (
    val token: String,
    val expirationTime: Long,
    val permissions: Set<Permissions>
)

data class TokenCredentials(
    val login: String,
    val expirationTime: Long,
    val permissions: Set<Permissions>
)

data class VerifiedTokenCredentials(
    val login: String,
    val expirationTime: Long,
    val permissions: Set<Permissions>,
    val allPermissions: Set<Permissions>
)
