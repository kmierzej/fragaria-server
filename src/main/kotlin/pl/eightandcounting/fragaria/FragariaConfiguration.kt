package pl.eightandcounting.fragaria

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import org.knowm.dropwizard.sundial.SundialConfiguration
import pl.eightandcounting.fragaria.repository.DatasetType
import pl.eightandcounting.fragaria.repository.tokenRepository.VerifyStrategy
import ru.vyarus.dropwizard.orient.configuration.HasOrientServerConfiguration
import ru.vyarus.dropwizard.orient.configuration.OrientServerConfiguration
import javax.validation.Valid
import javax.validation.constraints.NotNull

class FragariaConfiguration : Configuration(), HasOrientServerConfiguration {
    var secret: String = ""
    var tokenStrategy: VerifyStrategy = VerifyStrategy.WHITELISTING
    var enableCors = false
    var dataset = DatasetType.INITIAL

    @NotNull
    @Valid
    @JsonProperty("orient-server")
    private lateinit var orientServerConfiguration: OrientServerConfiguration

    override fun getOrientServerConfiguration(): OrientServerConfiguration {
        return orientServerConfiguration
    }

    @Valid
    @NotNull
    @JsonProperty("sundial")
    var sundialConfiguration = SundialConfiguration()
}
