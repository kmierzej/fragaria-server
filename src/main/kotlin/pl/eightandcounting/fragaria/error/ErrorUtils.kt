package pl.eightandcounting.fragaria.error

import javax.ws.rs.WebApplicationException

fun getUnknownProjectException(): WebApplicationException {
    return ErrorMessageBasic.UNKNOWN_PROJECT.toException()
}

fun getUnknownUserException(): WebApplicationException {
    return ErrorMessageBasic.UNKNOWN_USER.toException()
}

fun getUnknownTaskException(): WebApplicationException {
    return ErrorMessageBasic.UNKNOWN_TASK.toException()
}

fun getMissingParamException(name: String): WebApplicationException {
    return ErrorMessageExtended(ErrorMessageBasic.MISSING_PARAMS, name).toException();
}
