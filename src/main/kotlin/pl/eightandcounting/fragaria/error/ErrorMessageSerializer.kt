package pl.eightandcounting.fragaria.error

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

class ErrorMessageSerializer @JvmOverloads constructor(t: Class<ErrorMessage>? = null) : StdSerializer<ErrorMessage>(t) {
    override fun serialize(value: ErrorMessage, jgen: JsonGenerator, provider: SerializerProvider) {
        jgen.writeStartObject()
        jgen.writeStringField("code", value.name)
        jgen.writeStringField("description", value.description)
        jgen.writeEndObject()
    }
}
