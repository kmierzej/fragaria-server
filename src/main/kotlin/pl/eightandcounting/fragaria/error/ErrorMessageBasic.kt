package pl.eightandcounting.fragaria.error

import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(using = ErrorMessageSerializer::class)
enum class ErrorMessageBasic(val value: ErrorCode, override val description: String): ErrorMessage {
    LOGIN_FAIL_BAD_CREDENTIALS(ErrorCode.BAD_REQUEST, "Login lub Hasło nieprawidłowe"),
    DISABLED_ACCOUNT(ErrorCode.FORBIDDEN, "Twoje konto zostało zdezaktywowane. Aby uzyskać dostęp skontaktuj się z administratorem."),

    UNKNOWN_USER(ErrorCode.NOT_FOUND, "Taki użytkownik nie istnieje"),
    UNKNOWN_TASK(ErrorCode.NOT_FOUND, "Takie zadanie nie istnieje"),
    UNKNOWN_PROJECT(ErrorCode.NOT_FOUND, "Taki projekt nie istnieje"),

    CREATE_TIME_ENTRY_ERROR(ErrorCode.CONFLICT, "Błąd tworzenia logu czasowego. Spróbuj ponownie."),

    DATABASE_ERROR(ErrorCode.SERVER_ERROR, "Niespójny stan bazy. Powiadom Administratora"),
    EXCLUSIVE_QUERY_PARAMS_CHOSEN(ErrorCode.BAD_REQUEST, "Wybrano wzajemnie wykluczające się parametry zapytania"),
    INVALID_PATH_PARAMS(ErrorCode.BAD_REQUEST, "Błędne parametry ścieżki"),
    MISSING_PARAMS(ErrorCode.BAD_REQUEST, "Brak obowiązkowych parametrów"),
    PASSWORD_IS_TOO_SHORT(ErrorCode.BAD_REQUEST, "Zbyt krótkie hasło"),
    WRONG_PASSWORD(ErrorCode.BAD_REQUEST, "Podano nieprawidłowe stare hasło"),

    DUPLICATED_TITLE(ErrorCode.BAD_REQUEST, "Ten tytuł już istnieje"),

    FORBIDDEN_RESOURCE(ErrorCode.FORBIDDEN, "Brak uprawnień do zasobu"),
    LACK_OF_ADMIN_AUTH(ErrorCode.FORBIDDEN, "Zaloguj się jako administrator i spróbuj ponownie"),
    AUTH_ERROR(ErrorCode.UNAUTHORIZED, "Credentials are required to access this resource.")
    ;

    override val code: Int
        get() = this.value.value
}
