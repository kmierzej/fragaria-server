package pl.eightandcounting.fragaria.error

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

interface ErrorMessage {
    val code: Int
    val description: String
    val name: String

    fun toException() = WebApplicationException(this.toResponse())
    fun toResponse(): Response = Response.status(code).entity(this).type(MediaType.APPLICATION_JSON_TYPE).build()
}
