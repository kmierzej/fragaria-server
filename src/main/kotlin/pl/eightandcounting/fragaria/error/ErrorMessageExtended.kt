package pl.eightandcounting.fragaria.error

import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize(using = ErrorMessageSerializer::class)
class ErrorMessageExtended(message: ErrorMessage, moreInfo: String): ErrorMessage {
    override val code = message.code
    override val name = message.name
    override val description: String = createDescription(message.description, moreInfo)

    private fun createDescription(description: String, moreInfo: String): String {
        return "$description - $moreInfo"
    }
}
