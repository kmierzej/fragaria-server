package utils

import org.jose4j.jwt.NumericDate

fun NumericDate.addHours(hours: Long): NumericDate {
    val date = NumericDate.fromSeconds(this.value)
    date.addSeconds(60*60*hours)
    return date
}

fun NumericDate.addMinutes(minutes: Long): NumericDate {
    val date = NumericDate.fromSeconds(this.value)
    date.addSeconds(60*minutes)
    return date
}
