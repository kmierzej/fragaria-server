package pl.eightandcounting.fragaria.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.dropwizard.jackson.Jackson

val MAPPER = getMapper()

private fun getMapper(): ObjectMapper {
    val mapper = Jackson.newObjectMapper().registerKotlinModule()
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    return mapper
}

