package pl.eightandcounting.fragaria.api

import io.dropwizard.testing.FixtureHelpers.fixture
import org.apache.commons.lang3.StringUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class UserCredentialsUTest {
    val credentials = UserCredentials("ex@example.com", "password")
    val fixture: String = StringUtils.deleteWhitespace(fixture("fixtures/api/UserCredentials.json"))

    @Test
    fun serializesToJSON() {
        assertThat(MAPPER.writeValueAsString(credentials)).isEqualTo(fixture)
    }

    @Test
    fun deserializeFromJSON() {
        assertThat(MAPPER.readValue(fixture, credentials::class.java)).isEqualTo(credentials)
    }
}
