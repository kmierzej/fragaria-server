package pl.eightandcounting.fragaria.api

import io.dropwizard.testing.FixtureHelpers.fixture
import org.apache.commons.lang3.StringUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class UserIdentityUTest {
    val user = UserIdentity(
        login = "ex@example.com",
        email = null,
        fullname = "fullname",
        permissions = setOf(Permissions.ADMIN))

    @Test
    fun serializesToJSON() {
        assertThat(MAPPER.writeValueAsString(user)).isEqualTo(getFixture("UserIdentity"))
    }

    @Test
    fun deserializesFromJSON() {
        assertThat(MAPPER.readValue(getFixture("UserIdentityDeserialize"), user::class.java))
            .isEqualTo(user.copy(email = "email"))
    }

    @Test
    fun deserializesFromOptionalJSON() {
        val user = user.copy(
            fullname = null,
            email = null,
            permissions = null
        )
        assertThat(MAPPER.readValue(getFixture("UserIdentityDeserializeOptional"), user::class.java))
            .isEqualTo(user)
    }

    private fun getFixture(file: String): String {
        return StringUtils.deleteWhitespace(fixture("fixtures/api/$file.json"))
    }
}
