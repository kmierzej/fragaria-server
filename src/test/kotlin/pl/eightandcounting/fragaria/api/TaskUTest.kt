package pl.eightandcounting.fragaria.api

import io.dropwizard.testing.FixtureHelpers.fixture
import org.apache.commons.lang3.StringUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class TaskUTest {
    val task = Task(
        id =  "AT-5",
        title = "title",
        description = "description",
        status = Task.Status.OPEN,
        created = null,
        updated = null,
        totalLogged = null,
        estimatedTime = null
    )

    @Test
    fun serializesToJSON() {
        val fromObject = MAPPER.writeValueAsString(task)
        val fixture = getFixture("Task")
        assertThat(fromObject).isEqualTo(fixture)
    }

     @Test
    fun deserializeFromJSON() {
        val fixture = getFixture(("Task"))
        val fromString = MAPPER.readValue(fixture, Task::class.java)
        assertThat(fixture).isEqualTo(MAPPER.writeValueAsString(fromString))
    }

    private fun getFixture(file: String): String {
        return StringUtils.deleteWhitespace(fixture("fixtures/api/$file.json"))
    }
}
