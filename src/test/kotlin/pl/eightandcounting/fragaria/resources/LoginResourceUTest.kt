package pl.eightandcounting.fragaria.resources

import org.assertj.core.api.Assertions.assertThat
import org.jose4j.jwt.NumericDate
import org.junit.Test
import pl.eightandcounting.fragaria.RequestAddress
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.Permissions.*
import pl.eightandcounting.fragaria.api.UserCredentials
import java.util.*
import javax.ws.rs.WebApplicationException

private val now = NumericDate.now()
private val resource = getLoginResource { now }
private val FULL_SET_PERM = EnumSet.allOf(Permissions::class.java)
private val FULL_LOW_PERM = EnumSet.of(USER, ACCOUNTANT)

private val requestAddress = RequestAddress("address", "userAgent")

class LoginResourceUTest {
    @Test
    fun successfulUserLogin() {
        val requested = setOf(USER)
        val userCredentials = UserCredentials(FULL_SET, "")
        val result = resource.login(requestAddress, userCredentials, requested)

        assertThat(result.login).isEqualTo(userCredentials.login)
        assertThat(result.allPerm).isEqualTo(FULL_SET_PERM)
        assertThat(result.refreshToken).isNotNull()
        assertThat(result.refreshToken!!.permissions).containsOnly(USER)
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(USER) }
    }

    @Test
    fun successfulEmptyRequestedLogin() {
        val requested = emptySet<Permissions>()
        val userCredentials = UserCredentials(FULL_SET, "")
        val result = resource.login(requestAddress, userCredentials, requested)

        assertThat(result.login).isEqualTo(userCredentials.login)
        assertThat(result.allPerm).isEqualTo(FULL_SET_PERM)
        assertThat(result.refreshToken).isNotNull()
        assertThat(result.refreshToken!!.permissions).containsOnly(USER)
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(USER) }
    }

    @Test
    fun successfulMixedLogin() {
        val requested = setOf(USER, ADMIN)
        val userCredentials = UserCredentials(FULL_SET, "")
        val result = resource.login(requestAddress, userCredentials, requested)

        assertThat(result.login).isEqualTo(userCredentials.login)
        assertThat(result.allPerm).isEqualTo(FULL_SET_PERM)
        assertThat(result.refreshToken).isNotNull()
        assertThat(result.refreshToken!!.permissions).containsOnly(ADMIN)
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(ADMIN) }
    }

    @Test
    fun downgradedLogin() {
        val requested = FULL_LOW_PERM
        val userCredentials = UserCredentials(USER_SET, "")
        val result = resource.login(requestAddress, userCredentials, requested)

        assertThat(result.login).isEqualTo(userCredentials.login)
        assertThat(result.allPerm).isEqualTo(setOf(USER))
        assertThat(result.refreshToken).isNotNull()
        assertThat(result.refreshToken!!.permissions).containsOnly(USER)
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(USER) }
    }

    @Test(expected = WebApplicationException::class)
    fun noUserPermissionLogin() {
        val requested = setOf(USER)
        val userCredentials = UserCredentials(EMPTY_SET, "")
        resource.login(requestAddress, userCredentials, requested)
    }

    @Test(expected = WebApplicationException::class)
    fun unknownUserLogin() {
        val requested = setOf(USER)
        val userCredentials = UserCredentials("", "")
        resource.login(requestAddress, userCredentials, requested)
    }

    @Test(expected = WebApplicationException::class)
    fun noAdminPermissionLogin() {
        val requested = setOf(ADMIN)
        val userCredentials = UserCredentials(LOW_PRIV_SET, "")
        resource.login(requestAddress, userCredentials, requested)
    }

    @Test(expected = WebApplicationException::class)
    fun saveTokenFailureLogin() {
        val requested = setOf(USER)
        val userCredentials = UserCredentials(LOW_PRIV_SET + SAVE_FAILURE, "")
        resource.login(requestAddress, userCredentials, requested)
    }
}
