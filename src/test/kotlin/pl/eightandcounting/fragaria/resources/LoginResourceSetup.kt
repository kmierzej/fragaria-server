package pl.eightandcounting.fragaria.resources

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.mock
import org.jose4j.jwt.NumericDate
import pl.eightandcounting.fragaria.api.Permissions
import pl.eightandcounting.fragaria.api.UserCredentials
import pl.eightandcounting.fragaria.model.auth.TokenCredentials
import pl.eightandcounting.fragaria.model.auth.VerifiedTokenCredentials
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenRepository
import pl.eightandcounting.fragaria.repository.tokenRepository.TokenVerifyRepository
import pl.eightandcounting.fragaria.services.crypt.HmacKeyProvider
import pl.eightandcounting.fragaria.services.crypt.StringHasher
import pl.eightandcounting.fragaria.services.login.LoginService
import pl.eightandcounting.fragaria.services.login.LoginService.PermissionContainer
import pl.eightandcounting.fragaria.services.token.TokenBuilderServiceImpl
import pl.eightandcounting.fragaria.services.token.TokenReaderService

// verification
val VERIFICATION_FAILURE = ""
val PASS_VERIFICATION = "passVerification"
val DOWNGRADE_TO_USER = "downgradeToUser"
val NO_PERMISSIONS = "noPermissions"

// save token
val SAVE_FAILURE = "saveFailure"

// extracted credentials / login
val READ_FAILURE = "readFailure"
val USER_SET = "userSet"
val LOW_PRIV_SET = "lowPrivSet"
val ADMIN_SET = "adminSet"
val EMPTY_SET = "emptySet"
val FULL_SET = "fullSet"

private val verificator = mock<TokenVerifyRepository> {
    on { verifyToken(any(), any()) } doAnswer {
        val cred = it.arguments[0] as TokenCredentials
        val login = cred.login

        when {
            login.contains(PASS_VERIFICATION) -> VerifiedTokenCredentials(login, cred.expirationTime, cred.permissions, cred.permissions)
            login.contains(DOWNGRADE_TO_USER) -> VerifiedTokenCredentials(login, cred.expirationTime, setOf(Permissions.USER), setOf(Permissions.USER))
            login.contains(NO_PERMISSIONS) -> VerifiedTokenCredentials(login, cred.expirationTime, emptySet(), emptySet())
            else -> null
        }
    }
}

private val saver = mock<TokenRepository> {
    on { saveToken(any(), any())} doAnswer {
        val login = it.arguments[0] as String
        (!login.contains(SAVE_FAILURE))
    }
}

private val login = mock<LoginService> {
    on { login(any(), any()) } doAnswer {
        val cred = it.arguments[0] as UserCredentials
        val requested = it.arguments[1] as Set<Permissions>

        val all = getPermissions(cred.login)
        if(all == null) {
            null
        } else {
            val granted = requested.toMutableSet()
            granted.retainAll(all)
            PermissionContainer(granted, all)
        }
    }
}

private val reader = mock<TokenReaderService> {
    on { validateAccessToken(any()) } doAnswer {
        val token = it.arguments[0] as String
        extractTokenCredentials(token, 5)
    }
    on { validateRefreshToken(any()) } doAnswer {
        val token = it.arguments[0] as String
        extractTokenCredentials(token, 24)
    }
}

private val hasher = mock<StringHasher> {
    on { hashString(any()) } doAnswer {
        it.arguments[0] as String
    }
}

private fun extractTokenCredentials(wholeString: String, time: Long): TokenCredentials? {
    val token = wholeString.substringBeforeLast(':')
    val login = wholeString.substringAfterLast(':')
    val permissions = getPermissions(token) ?: return null
    return TokenCredentials(login, time, permissions)
}

private fun getPermissions(login: String): Set<Permissions>? {
    return when {
            login.contains(READ_FAILURE) -> null
            login.contains(USER_SET) -> setOf(Permissions.USER)
            login.contains(LOW_PRIV_SET) -> setOf(Permissions.USER, Permissions.ACCOUNTANT)
            login.contains(ADMIN_SET) -> setOf(Permissions.ADMIN)
            login.contains(EMPTY_SET) -> emptySet<Permissions>()
            login.contains(FULL_SET) -> setOf(Permissions.USER, Permissions.ADMIN, Permissions.ACCOUNTANT)
            else -> null
    }
}

fun getLoginResource(dateProvider: () -> NumericDate): LoginResource {
    val keys = HmacKeyProvider("secret").provide()
    return LoginResource(login, saver, verificator, TokenBuilderServiceImpl(keys, dateProvider, true), reader, hasher)
}
