package pl.eightandcounting.fragaria.resources

import org.assertj.core.api.Assertions.assertThat
import org.jose4j.jwt.NumericDate
import org.junit.Test
import pl.eightandcounting.fragaria.RequestAddress
import pl.eightandcounting.fragaria.api.Permissions.USER
import pl.eightandcounting.fragaria.resources.LoginResource.RefreshUserBody
import javax.ws.rs.WebApplicationException

private val now = NumericDate.now()
private val resource = getLoginResource { now }
private val requestAddress = RequestAddress("address", "userAgent")

class RefreshAccessTokenUTest {
    @Test
    fun successfulPlainRefresh() {
        val requested = setOf(USER)
        val body = RefreshUserBody(USER_SET + ":" + PASS_VERIFICATION)
        val result = resource.refreshAccessToken(requestAddress, body, requested)
        assertThat(result).isNotNull()

        assertThat(result.allPerm).isEqualTo(requested)
        assertThat(result.refreshToken).isNull()
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(USER) }
    }

    @Test
    fun changedPermissions() {
        val requested = setOf(USER)
        val body = RefreshUserBody(FULL_SET + ":" + DOWNGRADE_TO_USER)
        val result = resource.refreshAccessToken(requestAddress, body, requested)
        assertThat(result).isNotNull()

        assertThat(result.allPerm).isEqualTo(requested)
        assertThat(result.refreshToken).isNotNull()
        assertThat(result.refreshToken!!.permissions).containsOnly(USER)
        assertThat(result.accessTokens).isNotNull().hasSize(1).hasOnlyOneElementSatisfying { it.permissions == setOf(USER) }
    }

    @Test(expected = WebApplicationException::class)
    fun failVerification() {
        val requested = setOf(USER)
        val body = RefreshUserBody(USER_SET + ":" + VERIFICATION_FAILURE)
        resource.refreshAccessToken(requestAddress, body, requested)
    }

    @Test(expected = WebApplicationException::class)
    fun failDecoding() {
        val requested = setOf(USER)
        val body = RefreshUserBody(READ_FAILURE + ":" + PASS_VERIFICATION)
        resource.refreshAccessToken(requestAddress, body, requested)
    }
}
