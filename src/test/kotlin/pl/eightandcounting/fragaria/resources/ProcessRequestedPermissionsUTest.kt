package pl.eightandcounting.fragaria.resources

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import pl.eightandcounting.fragaria.api.Permissions
import javax.ws.rs.WebApplicationException


class ProcessRequestedPermissionsUTest {
    @Test
    fun processMixedPermissions() {
        val permissions = setOf(Permissions.ADMIN, Permissions.USER)
        val (default, pruned) = processRequestedPermissions(permissions)

        assertThat(default).isEqualTo(Permissions.ADMIN)
        assertThat(pruned).hasSize(1).contains(Permissions.ADMIN)
    }

    @Test(expected = WebApplicationException::class)
    fun processEmptyPermissions() {
        processRequestedPermissions(emptySet())
    }

    @Test
    fun processWithUserPermissions() {
        val permissions = setOf(Permissions.ACCOUNTANT, Permissions.USER)
        val (default, pruned) = processRequestedPermissions(permissions)

        assertThat(default).isEqualTo(Permissions.USER)
        assertThat(pruned).hasSize(2).contains(Permissions.USER).contains(Permissions.ACCOUNTANT)
    }

    @Test
    fun processWithoutUserPermissions() {
        val permissions = setOf(Permissions.ACCOUNTANT)
        val (default, pruned) = processRequestedPermissions(permissions)

        assertThat(default).isEqualTo(Permissions.ACCOUNTANT)
        assertThat(pruned).hasSize(1).contains(Permissions.ACCOUNTANT)
    }
}
