package pl.eightandcounting.fragaria.migrations

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mindrot.jbcrypt.BCrypt
import pl.eightandcounting.fragaria.repository.DatasetType
import java.io.InputStream
import java.io.StringReader


class MigrationReaderUTest {
    val migrations = getFile("demo")

    val emptyStream = "\n\n".byteInputStream()
    val duplicateStream = "#1\n#1\n#1".byteInputStream()
    val wrongOrderStream = "#2\n#1".byteInputStream()
    val emptyMigrationStream = "#1\n#2".byteInputStream()
    val transformPasswordStream = "#1\n<password:admin>".byteInputStream()

    @Test
    fun parseMigrationFromZero() {

        val migration = MigrationReader(migrations, 0)

        val first = migration.next()
        assertThat(first.metadata).isEqualTo(MigrationReader.MigrationMetadata(1))
        assertThat(first.migrations.size).isEqualTo(1)

        val second = migration.next()
        assertThat(second.metadata).isEqualTo(MigrationReader.MigrationMetadata(2))
        assertThat(second.migrations.size).isEqualTo(2)

        val third = migration.next()
        assertThat(third.metadata).isEqualTo(MigrationReader.MigrationMetadata(3, DatasetType.DEMO))
        assertThat(third.migrations.size).isEqualTo(0)
    }

    @Test
    fun emptyMigrations() {
        val migration = MigrationReader(emptyMigrationStream, 0)

        val first = migration.next()
        assertThat(first.metadata).isEqualTo(MigrationReader.MigrationMetadata(1))
        assertThat(first.migrations.size).isEqualTo(0)

        val second = migration.next()
        assertThat(second.metadata).isEqualTo(MigrationReader.MigrationMetadata(2))
        assertThat(second.migrations.size).isEqualTo(0)
        assertThat(migration.hasNext()).isFalse()
    }

    @Test
    fun emptyMigrationsLast() {
        val migration = MigrationReader(emptyMigrationStream, 1)

        assertThat(migration.hasNext()).isTrue()
        val second = migration.next()
        assertThat(second.metadata).isEqualTo(MigrationReader.MigrationMetadata(2))
        assertThat(second.migrations.size).isEqualTo(0)
        assertThat(migration.hasNext()).isFalse()
    }

    @Test
    fun transformPassword() {
        val migration = MigrationReader(transformPasswordStream, 0)
        assertThat(migration.hasNext()).isTrue()
        val result = migration.next()
        assertThat(result.migrations.size).isEqualTo(1)
        val query = result.migrations.first()
        val check = BCrypt.checkpw("admin", query)
        assertThat(check).isTrue()
    }

    @Test
    fun hasNextFromZero() {
        val migration = MigrationReader(migrations, 0)
        assertThat(migration.hasNext()).isTrue()
    }

    @Test
    fun parseMigrationFromSecond() {
        val migration = MigrationReader(migrations, 1)

        val second = migration.next()
        assertThat(second.metadata).isEqualTo(MigrationReader.MigrationMetadata(2))
        assertThat(second.migrations.size).isEqualTo(2)
    }

    @Test
    fun hasNextAfterSecond() {
        val migration = MigrationReader(migrations, 2)
        assertThat(migration.hasNext()).isTrue()
        migration.next()
        assertThat(migration.hasNext()).isFalse()
    }

    @Test(expected = NoSuchElementException::class)
    fun parseMigrationFromNewest() {
        val migration = MigrationReader(migrations, 3)
        migration.next()
    }

    @Test
    fun hasNextFromNewest() {
        val migration = MigrationReader(migrations, 3)
        assertThat(migration.hasNext()).isFalse()
    }

    @Test(expected = NoSuchElementException::class)
    fun nextOnEmptyFile() {
        StringReader("")
        val migration = MigrationReader(emptyStream, 0)
        migration.next()
    }

    @Test
    fun hasNextOnEmpty() {
        val migration = MigrationReader(emptyStream, 0)
        assertThat(migration.hasNext()).isFalse()
    }

    @Test(expected = Error::class)
    fun errorOnDuplicate() {
        val migration = MigrationReader(duplicateStream, 0)
        migration.next()
        migration.next()
    }

    @Test(expected = Error::class)
    fun errorOnWrongOrder() {
        val migration = MigrationReader(wrongOrderStream, 0)
        migration.next()
        migration.next()
    }

    private fun getFile(file: String): InputStream {
        return this::class.java.classLoader.getResourceAsStream("fixtures/migrations/$file.sql")
    }
}
