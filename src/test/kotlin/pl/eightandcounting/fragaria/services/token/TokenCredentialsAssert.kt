package pl.eightandcounting.fragaria.services.token

import org.assertj.core.api.AbstractAssert
import org.assertj.core.api.Assertions.assertThat
import org.jose4j.jwt.NumericDate
import pl.eightandcounting.fragaria.model.auth.Token
import pl.eightandcounting.fragaria.model.auth.TokenCredentials
import utils.addHours
import utils.addMinutes
import java.util.*

fun assertThat(actual: TokenCredentials): TokenCredentialsAssert {
    return TokenCredentialsAssert(actual)
}

class TokenCredentialsAssert(actual: TokenCredentials) : AbstractAssert<TokenCredentialsAssert, TokenCredentials>(actual, TokenCredentialsAssert::class.java) {
    fun hasLogin(login: String): TokenCredentialsAssert {
        if (!Objects.equals(actual.login, login)) {
            failWithMessage("Expected token's name to be <%s> but was <%s>", login, actual.login)
        }
        return this
    }

    fun matchesToken(token: Token): TokenCredentialsAssert {
        assertThat(actual.expirationTime).isEqualTo(token.expirationTime)
        assertThat(actual.permissions).hasSameElementsAs(token.permissions)

        return this
    }

    fun isAccessToken(now: NumericDate): TokenCredentialsAssert {
        assertThat(actual.expirationTime).isEqualTo(now.addMinutes(5).valueInMillis)
        return this
    }

    fun isRefreshToken(now: NumericDate): TokenCredentialsAssert {
        assertThat(actual.expirationTime).isEqualTo(now.addHours(24).valueInMillis)
        return this
    }
}
