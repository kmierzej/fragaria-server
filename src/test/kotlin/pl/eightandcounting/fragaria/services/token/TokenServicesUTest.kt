package pl.eightandcounting.fragaria.services.token

import org.assertj.core.api.Assertions.*
import org.jose4j.keys.HmacKey
import org.junit.Test
import pl.eightandcounting.fragaria.services.crypt.HmacKeyProvider.HmacPair

const val login = "admin"
val MIXED_PERMISSIONS_SET: Set<pl.eightandcounting.fragaria.api.Permissions> = setOf(pl.eightandcounting.fragaria.api.Permissions.ADMIN, pl.eightandcounting.fragaria.api.Permissions.USER)


class TokenBuilderServiceUTest {
    //test data setup
    val now: org.jose4j.jwt.NumericDate = org.jose4j.jwt.NumericDate.now()
    val keys = getKeyPair()

    //tested services setup
    val tokenBuilder = pl.eightandcounting.fragaria.services.token.TokenBuilderServiceImpl(keys, { now })
    val tokenReader = pl.eightandcounting.fragaria.services.token.TokenReaderServiceImpl(keys, { now })

    @Test
    fun refreshUserAccessToken() {
        val token = tokenBuilder.createAccessTokens(pl.eightandcounting.fragaria.services.token.login, pl.eightandcounting.fragaria.api.Permissions.Companion.USER_PERMISSION_SET).first()
        val data = tokenReader.validateAccessToken(token.token) ?: validateFailed()
        assertThat(data).hasLogin(pl.eightandcounting.fragaria.services.token.login).matchesToken(token)
    }

    @Test
    fun refreshAdminAccessToken() {
        val token = tokenBuilder.createAccessTokens(pl.eightandcounting.fragaria.services.token.login, pl.eightandcounting.fragaria.services.token.MIXED_PERMISSIONS_SET)
        assertThat(token).hasSize(2)

        val adminToken = token.first { it.permissions.contains(pl.eightandcounting.fragaria.api.Permissions.ADMIN) }
        val userToken = token.first { it.permissions.contains(pl.eightandcounting.fragaria.api.Permissions.USER) }

        val adminData = tokenReader.validateAccessToken(adminToken.token) ?: validateFailed()
        val userData = tokenReader.validateAccessToken(userToken.token) ?: validateFailed()

        assertThat(adminData).hasLogin(pl.eightandcounting.fragaria.services.token.login).matchesToken(adminToken).isAccessToken(now)
        assertThat(userData).hasLogin(pl.eightandcounting.fragaria.services.token.login).matchesToken(userToken).isAccessToken(now)
    }

    @Test
    fun refreshAdminRefreshToken() {
        val token = tokenBuilder.refreshRefreshToken(pl.eightandcounting.fragaria.services.token.login, pl.eightandcounting.fragaria.services.token.MIXED_PERMISSIONS_SET, setOf(pl.eightandcounting.fragaria.api.Permissions.ADMIN))
        assertThat(token.accessTokens).hasSize(1)
        val accessToken = token.accessTokens.first()

        val refresh = tokenReader.validateRefreshToken(token.refreshToken.token) ?: validateFailed()
        val access = tokenReader.validateAccessToken(accessToken.token) ?: validateFailed()

        assertThat(refresh).hasLogin(pl.eightandcounting.fragaria.services.token.login).matchesToken(token.refreshToken).isRefreshToken(now)
        assertThat(access).hasLogin(pl.eightandcounting.fragaria.services.token.login).matchesToken(accessToken).isAccessToken(now)
    }

    @Test
    fun invalidKeyDecryption() {
        val token = tokenBuilder.refreshRefreshToken(pl.eightandcounting.fragaria.services.token.login, pl.eightandcounting.fragaria.services.token.MIXED_PERMISSIONS_SET, setOf(pl.eightandcounting.fragaria.api.Permissions.ADMIN))
        assertThat(token.accessTokens).hasSize(1)
        val accessToken = token.accessTokens.first()

        val refresh = tokenReader.validateRefreshToken(accessToken.token)
        val access = tokenReader.validateAccessToken(token.refreshToken.token)

        assertThat(refresh).isNull()
        assertThat(access).isNull()
    }

    private fun getKeyPair(): HmacPair {
        val accessSeed = ByteArray(64)
        val refreshSeed = ByteArray(64)
        refreshSeed.fill(1)
        accessSeed.fill(0)

        return HmacPair(HmacKey(accessSeed), HmacKey(refreshSeed))
    }

    private fun validateFailed(): Nothing {
        fail("validationFailed")
        throw Exception()
    }
}
