package pl.eightandcounting.fragaria.repository.query

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import pl.eightandcounting.fragaria.repository.query.PagedResourceQueryBuilder.Filter.Companion.simpleInClause

class PagedResourceQueryBuilderTest {
    val queryBuilder = PagedResourceQueryBuilder(
        key = "id",
        clazz = "Project",
        projections = arrayOf("*")
    )

    @Test
    fun getQueryProjectAll() {
        val query = queryBuilder.getQuery("admin")
        assertThat(query.params).hasSize(1)
        assertThat(query.params).containsEntry("key", "admin")
        assertThat(query.query).isEqualTo("SELECT * FROM Project WHERE id = :key")
    }

    @Test
    fun getQueryProjectWithKey() {
        val builder = PagedResourceQueryBuilder(
            key = "id",
            clazz = "Project",
            projections = arrayOf("id", "title")
        )

        val query = builder.getQuery("admin")
        assertThat(query.params).hasSize(1)
        assertThat(query.params).containsEntry("key", "admin")
        assertThat(query.query).isEqualTo("SELECT id, title FROM Project WHERE id = :key")
    }

    @Test
    fun getQueryProjectWithoutKey() {
        val builder = PagedResourceQueryBuilder(
            key = "id",
            clazz = "Project",
            projections = arrayOf("title")
        )

        val query = builder.getQuery("admin")
        assertThat(query.params).hasSize(1)
        assertThat(query.params).containsEntry("key", "admin")
        assertThat(query.query).isEqualTo("SELECT title, id FROM Project WHERE id = :key")
    }

    @Test
    fun getNextProjectWithOneFilter() {
        val filter = simpleInClause("status", arrayOf("OPEN"))
        val query = queryBuilder.getAfterQuery("user", 4, filter)
        assertThat(query.params).hasSize(3)
        assertThat(query.params).containsEntry("key", "user")
        assertThat(query.params).containsEntry("limit", 4)
        assertThat(query.params).containsEntry("status", arrayOf("OPEN"))
        assertThat(query.query).isEqualTo("SELECT * FROM Project WHERE status IN :status AND id > :key ORDER BY id ASC LIMIT :limit")
    }

    @Test
    fun getFromProjectWithOneFilter() {
        val filter = simpleInClause("status", arrayOf("OPEN"))
        val query = queryBuilder.getFromQuery("user", 4, filter)
        assertThat(query.params).hasSize(3)
        assertThat(query.params).containsEntry("key", "user")
        assertThat(query.params).containsEntry("limit", 4)
        assertThat(query.params).containsEntry("status", arrayOf("OPEN"))
        assertThat(query.query).isEqualTo("SELECT * FROM Project WHERE status IN :status AND id >= :key ORDER BY id ASC LIMIT :limit")
    }

    @Test
    fun getBeforeProjectWithMultipleFilters() {
        val filter = simpleInClause("status", arrayOf("OPEN", "CLOSED"))
        val query = queryBuilder.getBeforeQuery("user", 4, filter)
        assertThat(query.params).hasSize(3)
        assertThat(query.params).containsEntry("key", "user")
        assertThat(query.params).containsEntry("limit", 4)
        assertThat(query.params).containsEntry("status", arrayOf("OPEN", "CLOSED"))
    }
}
