# Fragaria Server #

### What is it? ###

Fragaria server is a backend for https://bitbucket.org/kmierzej/fragaria-webapp/overview.

It is time tracking application for small to medium not-IT companies.

### How to set it up? ###

At first you have to have access to fragaria-webapp webjar.
Go to the [repository ](https://bitbucket.org/kmierzej/fragaria-webapp/overview), and build it.

To run it invoke:
```
./gradlew run
```

To create package for end user invoke:
```
./gradlew distShadowZip
```

Package will be located in build/distributions.
To run it first edit config.yml file.

You can find config manuals here:

* [Dropwizard](http://www.dropwizard.io/0.7.1/docs/manual/configuration.html) configuration
* [OrientDb](https://github.com/xvik/dropwizard-orient-server) (chapter configuration) configuration
* All application specific options are described using comments in config.yml

If you want to run application with secure and performant HTTP2 protocol, you have to set up some things first. Otherwise remove HTTP2 from configuration, as it will break the build.

1. Download ALPN version mathing your JRE version: http://www.eclipse.org/jetty/documentation/current/alpn-chapter.html#alpn-versions
2. Setup keystore (check out: https://letsencrypt.org/ ), and add proper path to configuration.
3. Add following parameter on JVM startup: -Xbootclasspath/p:<path_to_alpn_boot_jar>

Run:
```
./server server <link to config.yml file>
```

With default configuration you will have access to those endpoints:
* [application](localhost:8080)
* [admin backend](localhost:8081)
* [database backend](localhost:8081/orient)

### How to run tests? ###

```
./gradlew test
```

### Used technologies: ###

* program written in Kotlin
* dropwizard
* orientdb